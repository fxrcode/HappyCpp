/*************************************************************************
	> File Name: basic.c
	> Author: 
	> Mail: 
	> Created Time: 2016年10月06日 星期四 02时56分13秒
 ************************************************************************/

#include<stdio.h>

int dummy()
{
    return 42;
}

int main()
{
    int a, b, c = dummy();
    printf("I got : %d, %d, %d\n", a, b, c);
    return 0;
}
