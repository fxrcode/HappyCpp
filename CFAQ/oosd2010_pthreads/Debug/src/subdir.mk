################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/cancellation.cpp \
../src/mutexes.cpp \
../src/prodcons.cpp \
../src/pthreads.cpp \
../src/threads.cpp 

OBJS += \
./src/cancellation.o \
./src/mutexes.o \
./src/prodcons.o \
./src/pthreads.o \
./src/threads.o 

CPP_DEPS += \
./src/cancellation.d \
./src/mutexes.d \
./src/prodcons.d \
./src/pthreads.d \
./src/threads.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


