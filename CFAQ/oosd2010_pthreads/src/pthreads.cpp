//============================================================================
// Name        : pthreads.cpp
// Author      : G Lipari
// Version     :
// Copyright   : OOSD
// Description : Hello World in C++, Ansi-style
//============================================================================

#include "threads.hpp"
#include "cancellation.hpp"
#include "mutexes.hpp"
#include "prodcons.hpp"
#include <iostream>
using namespace std;

int main() {
	cout << "************ THREAD EXAMPLE **************" << endl;
	thread_example::run();
	cout << "********* CANCELLATION EXAMPLE ***********" << endl;
	canc_example::run();
	cout << "************ MUTEX EXAMPLE ***************" << endl;
	mutex_example::main();
	cout << "********** PROD/CONS EXAMPLE *************" << endl;
	prod_cons_example::main();

	return 0;
}
