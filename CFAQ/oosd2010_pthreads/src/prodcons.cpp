/*
 * prodcons.cpp
 *
 *  Created on: Apr 28, 2011
 *      Author: lipari
 */

#include "prodcons.hpp"
#include <pthread.h>
#include <iostream>
#include <vector>

using namespace std;

#define N1 4
#define N2 3

namespace prod_cons_example {
	class MailBox {
		mutable pthread_mutex_t m;
		pthread_cond_t empty;
		pthread_cond_t full;
		vector<int> array;
		unsigned head, tail;
		unsigned num;
		unsigned max;
	public:
		MailBox(int nelem = 4) : array(nelem), head(0), tail(0), num(0), max(nelem)
		{
			pthread_mutex_init(&m, 0);
			pthread_cond_init(&empty, 0);
			pthread_cond_init(&full, 0);
		}
		void insert(int elem) {
			pthread_mutex_lock(&m);
			while (num == max) pthread_cond_wait(&full, &m);
			array[head] = elem;
			head = (head + 1) % max;
			num++;
			pthread_cond_signal(&empty);
			pthread_mutex_unlock(&m);
		}

		int extract() {
			pthread_mutex_lock(&m);
			while (num == 0) pthread_cond_wait(&empty, &m);
			int elem = array[tail];
			tail = (tail +1) % max;
			num --;
			pthread_cond_signal(&full);
			pthread_mutex_unlock(&m);
			return elem;
		}

		unsigned size() const {
			return num;
		}

		unsigned free_slots() const {
			pthread_mutex_lock(&m);
			unsigned f = max - num;
			pthread_mutex_unlock(&m);
			return f;
		}
	};

	MailBox mail;

	void *producer(void *arg)
	{
		int index = *((int *)arg);
		for (int i=0; i<10; i++) {
			for (int k=0; k<50000; k++);
			int elem = index*10+i;
			cout << "Producer " << index << " produced " << elem << endl;
			mail.insert(elem);
		}
		return 0;
	}

	void *consumer(void *arg)
	{
		int index = *((int *)arg);
		for (int i=0; i<10; i++) {
			int elem = mail.extract();
			cout << "Consumer " << index << " received " << elem << endl;
			for (int k=0; k<50000; k++);
		}
		return 0;
	}

	int main()
	{
		pthread_t tid1[N1];
		pthread_t tid2[N2];
		int arg1[N1];
		int arg2[N2];

		for (int i=0; i<N1; i++) {
			arg1[i] = i;
			pthread_create(&tid1[i], 0, producer, &arg1[i]);
		}
		for (int i=0; i<N2; i++) {
			arg2[i] = i;
			pthread_create(&tid2[i], 0, consumer, &arg2[i]);
		}

		cout << "Main: producers and consumers created" << endl;

		for (int i=0; i<N1; i++)
			pthread_join(tid1[i], 0);
		for (int i=0; i<N2; i++)
			pthread_join(tid2[i], 0);
	}
}
