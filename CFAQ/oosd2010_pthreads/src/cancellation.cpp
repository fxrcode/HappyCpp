/*
 * cancellation.cpp
 *
 *  Created on: Apr 28, 2011
 *      Author: lipari
 */

#include "cancellation.hpp"
#include <cstdio>
#include <pthread.h>

using namespace std;

namespace canc_example {
	void my_message(void *arg)
	{
	  printf("KILLED!!!\n");
	}

	int i;  // global variable!

	void *thread(void *arg)
	{
	  i = 0;

	  pthread_cleanup_push(my_message, NULL);

	  if (arg)
		pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

	  for (; i<50000000; i++); /* busy wait */

	  pthread_testcancel();

	  pthread_cleanup_pop(0);

	  printf("Not killed!!!\n");

	  return NULL;
	}

	void run()
	{
	  pthread_t mythread;

	  int err;

	  /* creates a thread with deferred cancellation */

	  err = pthread_create(&mythread, 0, thread, NULL);
	  pthread_join(mythread, NULL);
	  printf("No cancellation: i=%d\n",i);

	  err = pthread_create(&mythread, 0, thread, NULL);
	  pthread_cancel(mythread);
	  pthread_join(mythread, NULL);

	  printf("With cancellation (deferred): i=%d\n",i);

	  err = pthread_create(&mythread, 0, thread, (void *)1);
	  pthread_cancel(mythread);
	  pthread_join(mythread, NULL);
	  printf("With cancellation (asynchronous): i=%d\n",i);
	}
}
