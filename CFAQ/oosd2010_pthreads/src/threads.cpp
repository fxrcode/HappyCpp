/*
 * threads.cpp
 *
 *  Created on: Apr 28, 2011
 *      Author: lipari
 */

#include <iostream>
#include <pthread.h>
#include "threads.hpp"

using namespace std;

#define N 4

namespace thread_example {

	void *mythread(void *arg)
	{
		int index = *((int*)arg);
		cout << "I am thread " << index << endl;
		for (int i=0; i<1000000; i++);

		cout << "Thread " << index << " finishing" << endl;
		int *ptemp = new int;
		*ptemp = 2*index;
		return ptemp;
	}

	void run()
	{
		pthread_t tid[N];
		int args[N];

		for (int i=0; i<N; i++) {
			args[i] = i;
			pthread_create(&tid[i], 0, mythread, &args[i]);
		}
		cout << "This is the main, all threads have been created" << endl;
		for (int i=0; i<N; i++) {
			int *presult;
			pthread_join(tid[i], (void**)&presult);
			cout << "thread " << i << " result = " << *presult << endl;
			delete presult;
		}
 	}
}
