/*
 * prodcons.hpp
 *
 *  Created on: Apr 28, 2011
 *      Author: lipari
 */

#ifndef PRODCONS_HPP_
#define PRODCONS_HPP_

namespace prod_cons_example {
	int main();
}

#endif /* PRODCONS_HPP_ */
