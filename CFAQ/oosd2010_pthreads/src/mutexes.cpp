/*
 * mutexes.cpp
 *
 *  Created on: Apr 28, 2011
 *      Author: lipari
 */

#include "mutexes.hpp"
#include <cstdio>
#include <pthread.h>

using namespace std;

namespace mutex_example {
	pthread_mutex_t mymutex;

	void *body(void *arg)
	{
	  int i,j;

	  for (j=0; j<10; j++) {
		//pthread_mutex_lock(&mymutex);
		for (int k=0; k<4; k++) {
			printf("%c",(*(char *)arg));
			for (i=0; i<2500000; i++);
		}
		//pthread_mutex_unlock(&mymutex);
	  }

	  return NULL;
	}

	int main()
	{
	  pthread_t t1,t2,t3;
	  pthread_attr_t myattr;
	  int err;

	  pthread_mutex_init(&mymutex, 0);

	  err = pthread_create(&t1, 0, body, (void *)".");
	  err = pthread_create(&t2, 0, body, (void *)"#");
	  err = pthread_create(&t3, 0, body, (void *)"o");

	  pthread_join(t1, NULL);
	  pthread_join(t2, NULL);
	  pthread_join(t3, NULL);

	  printf("\n");

	  return 0;
	}
}
