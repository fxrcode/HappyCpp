/*
 * threads.hpp
 *
 *  Created on: Apr 28, 2011
 *      Author: lipari
 */

#ifndef THREADS_HPP_
#define THREADS_HPP_

namespace thread_example {
	void run();
}

#endif /* THREADS_HPP_ */
