#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef int (*tony)(int);

int test1(int a)
{
    int i = 3;
    return a + i;
}

int test2(int b)
{
    int i = 3;
    return b - i;
}

typedef struct  {
    char name[10];
    int val;
    tony pfunc;
} Class;

/*Class *play = malloc(sizeof(Class));*/
Class *play;

Class scull = {
    .name = "HELLO",
    .val = 9999,
    .pfunc = &test2,
};

int main()
{
    play = malloc(sizeof(Class));
    /*play->name = "Class_A";*/
    strcpy(play->name, "Class_A");
    play->val = 42;
    play->pfunc = &test1;
    
    int ans1 = play->pfunc(40);
    printf("I got %d\n", ans1);
    int ans2 = scull.pfunc(100);
    printf("I got %d\n", ans2);
    
    return 0;
}
