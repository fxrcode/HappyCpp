/*************************************************************************
	> File Name: thdMexp3.c
	> Author: 
	> Mail: 
	> Created Time: 2016年10月06日 星期四 06时53分47秒
        thread Argument Passing example 3: WRONG WAY 
************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>

#define NUM_THREADS 5

struct thread_data {
    int thread_id;
    int sum;
    char message[20];
};

const char MSG[5][20] = {
    "hello world",
    "ni hao",
    "sa yang he yo",
    "sa yo la la",
    "bonjue",
};

struct thread_data thread_data_array[NUM_THREADS];

void *PrintHello(void *threadarg)
{
    int val = *(int *)threadarg;
    printf("Hello from #%d\n", val);

    pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
    pthread_t threads[NUM_THREADS];
    int rc;
    int t;
    for (t = 0; t < NUM_THREADS; ++t) {
        printf("In main: creating thread %d\n", t);
        rc = pthread_create(&threads[t], NULL, PrintHello, (void *) &t);
        if (rc) {
            printf("ERRORR on pthread_create()\n");
            exit(-1);
        }
    }
    pthread_exit(NULL);
}
