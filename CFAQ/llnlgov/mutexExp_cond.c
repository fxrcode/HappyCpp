/*************************************************************************
	> File Name: mutexExp_cond.c
	> Author: 
	> Mail: 
	> Created Time: 2016年10月07日
 ************************************************************************/

#include<stdio.h>
#include <stdlib.h>
#include <pthread.h>

typedef struct 
{
    double  *a;
    double  *b;
    double  sum;
    int veclen;
} DOTDATA;

#define NUMTHRDS    4
#define VECLEN      100

DOTDATA dotstr;
pthread_t callThd[NUMTHRDS];
pthread_mutex_t mutexsum, mutexlvl;

/*The activated function shows the pattern of multi-thread*/
/*function. The input/output of the math is a global access*/
/*struct object, and the only arg is normally tid*/
void *dotprod(void *thdarg)
{
    pthread_mutex_lock (&mutexlvl);
    printf("Sync to start level: %ld\n", (long)thdarg);
    int i, start, end, len;
    long offset;
    double mysum, *x, *y;

    offset = (long)thdarg;
    len = dotstr.veclen;
    start = offset*len;
    end = start + len;
    x = dotstr.a;
    y = dotstr.b;

    mysum = 0;
    for (i = start; i < end; ++i) {
        mysum += (x[i] * y[i]);
    }

    /*Lock a mutex prior to updating the value in the shared struct*/
    /*and unlock it upon updating.*/
    pthread_mutex_lock (&mutexsum);
    dotstr.sum += mysum;
    pthread_mutex_unlock (&mutexsum);

    pthread_mutex_unlock (&mutexlvl);

    pthread_exit((void *)0);
}

int main (int argc, char *argv[])
{
    long i;
    double *a, *b;
    void *status;
    pthread_attr_t attr;

    a = (double *) malloc(NUMTHRDS * VECLEN * sizeof (double));
    b = (double *) malloc(NUMTHRDS * VECLEN * sizeof (double));

    for (i = 0; i < VECLEN * NUMTHRDS; ++i) {
        a[i] = 1.0f;
        b[i] = a[i];
    }

    dotstr.veclen = VECLEN;
    dotstr.a = a;
    dotstr.b = b;
    dotstr.sum = 0;

    pthread_mutex_init (&mutexsum, NULL);
    pthread_mutex_init (&mutexlvl, NULL); 
    /*Create threads to perform the dotproduct */
    pthread_attr_init (&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    for (i = 0; i < NUMTHRDS; ++i) {
        pthread_create(&callThd[i], &attr, dotprod, (void *)i);
    }

    pthread_attr_destroy(&attr);

    for (i = 0; i < NUMTHRDS; ++i) {
        pthread_join(callThd[i], &status);
    }

    printf("Sum = %f\n", dotstr.sum);
    free(a);
    free(b);
    pthread_mutex_destroy (&mutexlvl);
    pthread_mutex_destroy(&mutexsum);
    pthread_exit(NULL);
}
