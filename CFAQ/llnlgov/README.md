# mutexExp
* Understood why use mutex only on ``dotstr.sum += mysum``, since it is modifing global variable. And the threads execution is run out of order, since this is multi-core VM, it is really parallel running. So no way to determine the execution order every time. Must use sync methods.
* If I don't lock it, then the result is still correct. Because += is so fast than switching from each thread. 
* Why use ``pthread_join``? Ans: it is to let main thread to wait for worker threads to finish execution.
* How to sync threads to let threads run in order? I tried mutexlvl to lock each level, but it doesn't work as I wish.
