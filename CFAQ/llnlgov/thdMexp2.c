/*************************************************************************
	> File Name: thdMexp2.c
	> Author: 
	> Mail: 
	> Created Time: 2016年10月06日 星期四 06时53分47秒
        thread Argument Passing example 2
************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>

#define NUM_THREADS 5

struct thread_data {
    int thread_id;
    int sum;
    char message[20];
};

const char MSG[5][20] = {
    "hello world",
    "ni hao",
    "sa yang he yo",
    "sa yo la la",
    "bonjue",
};

struct thread_data thread_data_array[NUM_THREADS];

void *PrintHello(void *threadarg)
{
    struct thread_data *my_data;
    my_data = (struct thread_data *)threadarg;
    int td = my_data->thread_id;
    int sum = my_data->sum;
    char msg[20];
    strcpy(msg, my_data->message);
    printf("Hello from #%d, the message is %s\n", td, msg);

    pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
    pthread_t threads[NUM_THREADS];
    int rc;
    int t;
    for (t = 0; t < NUM_THREADS; ++t) {
        printf("In main: creating thread %d\n", t);
        thread_data_array[t].thread_id = t;
        thread_data_array[t].sum = t * t;
        strcpy(thread_data_array[t].message, MSG[t]); 
        rc = pthread_create(&threads[t], NULL, PrintHello, (void *) &thread_data_array[t]);
        if (rc) {
            printf("ERRORR on pthread_create()\n");
            exit(-1);
        }
    }
    pthread_exit(NULL);
}
