#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <utility>

#include <stdio.h>

#include <cpr/cpr.h>

using namespace std;

// debug level
#define DEBUG 1

class CameraCfg {
public:
    CameraCfg (const string&, const string&, const string&, const string&);
    ~CameraCfg ();
    int getCfg (bool);
    int setCfg (const string&);
    int getMAC(void);
    int parse(const string&);

private:
    string IP;
    string PATH;
    string CAMERA_PORT;
    ofstream myfile;
    vector< pair<string, string> > vp;
};

void err()
{
    exit(-1);
}

CameraCfg::CameraCfg (const string& ip, const string& path, const string& username, const string& password)
{
    IP = ip;
    PATH = path;
    CAMERA_PORT = "http://" + username + password + "@" + IP + "/cgi-bin/param_if.cgi";
}

CameraCfg::~CameraCfg(void)
{
    if (myfile.is_open())
        myfile.close();
}

/**
 * @brief      parse the config at loc into member vp for setCfg()
 *
 * @param[in]  loc   The location of the config file
 *
 * @return     errno
 */
int CameraCfg::parse(const string& loc)
{
    ifstream read(loc);
    string line;
    string temp[2];

    while (getline(read, line)) {
        int i = 0;
        istringstream iss(line);
        while (getline(iss, temp[i], '=')) {
            if (i == 0) {}
            else {
                if (temp[1].length() == 0)
                    vp.push_back(make_pair(temp[0], "NUL"));
                else
                    vp.push_back(make_pair(temp[0], temp[1]));
            }
            ++i;
        }
    }

    // print the parsed result when DEBUG ON
    #if DEBUG==2 
        for (auto &p : vp) {
            cout << p.first << "\t" << p.second << endl;
        }
    #endif

    if (vp.size() == 0)
        return -1;
    else 
        return 0;
}

/**
 * @brief      Gets the configuration.
 *
 * @param[in]  enable  enable to save cfg into PATH
 *
 * @return     errno
 */
int CameraCfg::getCfg (bool enable)
{
    #if DEBUG==1
        cout << "get Cfg setting...... " << endl;
    #endif
    auto rs = cpr::Get(cpr::Url{CAMERA_PORT},
    cpr::Parameters{
        {"NumActions", "13"},
        {"Action_0" , "ImageControls.ExposureMode.GetValue"},
        {"Action_1" , "ImageControls.ExposureOffset.GetValue"},
        {"Action_2" , "ImageControls.BacklightCompensation.GetValue"},
        {"Action_3" , "ImageControls.ExposureTimeLimit.GetValue"},
        {"Action_4" , "ImageControls.GainLimit_dB.GetValue"},
        {"Action_5" , "Stream.StreamSelector.GetValue"},
        {"Action_6" , "Stream.EncoderType.GetValue"},
        {"Action_7" , "Stream.OutputSize.GetValue"},
        {"Action_8" , "Stream.AOIWidth.GetValue"},
        {"Action_9",  "Stream.AOIHeight.GetValue"},
        {"Action_10", "SysInfo.ModelName.GetValue"},
        {"Action_11", "SysInfo.MACAddress.GetValue"},
        {"Action_12", "SysInfo.Serial.GetValue"}
    });
    if (rs.status_code != 200) {
        cout << "not working code = " << rs.status_code << "\n" << endl;
        return -1;
    }
    #ifdef DEBUG 
        // cout << rs.status_code << " " << rs.url << "\n" << endl;
        cout << rs.text << "\n" << endl;
    #endif

    if (!enable ) {
        return 0;
    }

    cout << "save Cfg setting into file : " << PATH << endl;
    if (!myfile.is_open())
        myfile.open(PATH);
    
    myfile << rs.text;
    myfile.close();
    return 0;
}

/**
 * @brief      Gets the modelName, MACaddr, SerialNum. For ping also.
 *
 * @return     The mac address.
 */
int CameraCfg::getMAC(void)
{
    auto rs = cpr::Get(cpr::Url{CAMERA_PORT},
    cpr::Parameters{
        {"NumActions", "3"},
        {"Action_0", "SysInfo.ModelName.GetValue"},
        {"Action_1", "SysInfo.MACAddress.GetValue"},
        {"Action_2", "SysInfo.Serial.GetValue"}
    });
    if (rs.status_code != 200) {
        cout << "not working code = " << rs.status_code << "\n" << endl;
        return -1;
    }
    cout << rs.text << "\n" << endl;
    return 0;
}

/**
 * @brief      Sets the configuration via parsing the cfg at loc at begining of setCfg
 *
 * @param[in]  loc   The location of cfg file
 *
 * @return     { errno }
 */
int CameraCfg::setCfg (const string& loc) 
{   
    #if DEBUG==1
        cout << "parse in setCfg using cfg file : " << loc << endl;
    #endif
    parse(loc);

    auto rs = cpr::Post(cpr::Url{CAMERA_PORT},
    cpr::Parameters{
        {"NumActions", "10"},
        {"Action_0" , "ImageControls.ExposureMode.SetValue"},
        {"Parameter_0_0", vp.at(0).second},
        {"Action_1" , "ImageControls.ExposureOffset.SetValue"},
        {"Parameter_1_0", vp.at(1).second},
        {"Action_2" , "ImageControls.BacklightCompensation.SetValue"},
        {"Parameter_2_0", vp.at(2).second},
        {"Action_3" , "ImageControls.ExposureTimeLimit.SetValue"},
        {"Parameter_3_0", vp.at(3).second},
        {"Action_4" , "ImageControls.GainLimit_dB.SetValue"},
        {"Parameter_4_0", vp.at(4).second},
        {"Action_5" , "Stream.StreamSelector.SetValue"},
        {"Parameter_5_0", vp.at(5).second},
        {"Action_6" , "Stream.EncoderType.SetValue"},
        {"Parameter_6_0", vp.at(6).second},
        {"Action_7" , "Stream.OutputSize.SetValue"},
        {"Parameter_7_0", vp.at(7).second},
        {"Action_8" , "Stream.AOIWidth.SetValue"},
        {"Parameter_8_0", vp.at(8).second},
        {"Action_9", "Stream.AOIHeight.SetValue"},
        {"Parameter_9_0", vp.at(9).second}
    });
    if (rs.status_code != 200) {
        cout << "not working code = " << rs.status_code << endl;
        return -1;
    }
    #if DEBUG==2
        cout << rs.status_code << " " << rs.url << endl;
    #endif

    return 0;
}