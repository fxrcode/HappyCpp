#include "cameraCfg.h"

/**
 * @brief      Example of using the cameraCfg class.
 *
 * @param[in]  argc  The argc
 * @param      argv  The argv
 *
 * @return     { description_of_the_return_value }
 */
int main (int argc, char** argv) {

    // Should pass IP, PATH string of config IP camera.
    const string IP = "192.168.0.173"; //"192.168.0.125:6688";
    const string PATH = "in.cfg";
    const string USERNAME = "admin";
    const string PASSWORD = "admin"; //"novumind";

    // Init the camera object with IP, PATH
    CameraCfg camIn0 (IP, PATH, USERNAME, PASSWORD);

    // Peek cfg into PATH.
    camIn0.getCfg (false);

    // simple ping to request MAC, serial number
    //camIn0.getMAC ();

    // Set cfg with a config file.
    if (camIn0.setCfg ("in2.cfg"))
        err();

    camIn0.getCfg (true);

    return 0;
}
