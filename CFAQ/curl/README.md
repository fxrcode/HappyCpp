## Modified cpr-example
* I used postman to test POST/GET request for Backlight setting.
* Then I tried the same in cpr-example, simply add parameters.

```
[~/HappyCpp/Network/cpr-example/build]
moocos-> ./example
Love tony
200 http://192.168.0.173/cgi-bin/param_if.cgi?NumActions=1&Action_0=ImageControls.BacklightCompensation.GetValue Return_0=0

```

## Build the 3 C++ curl wrappers
```
[~/HappyCpp/Network/restclient-cpp]
moocos-> sudo make install
m4 -I m4 -DM4_RESTCLIENT_VERSION=0.4.4-5-g01d5679 version.h.m4 > include/restclient-cpp/version.h
make  install-am
make[1]: Entering directory `/home/moocos/HappyCpp/Network/restclient-cpp'
m4 -I m4 -DM4_RESTCLIENT_VERSION=0.4.4-5-g01d5679 version.h.m4 > include/restclient-cpp/version.h
/bin/bash ./libtool  --tag=CXX   --mode=compile g++ -DHAVE_CONFIG_H -I.   -Iinclude -fPIC -g -O2 -MT source/librestclient_cpp_la-restclient.lo -MD -MP -MF source/.deps/librestclient_cpp_la-restclient.Tpo -c -o source/librestclient_cpp_la-restclient.lo `test -f 'source/restclient.cc' || echo './'`source/restclient.cc
libtool: compile:  g++ -DHAVE_CONFIG_H -I. -Iinclude -fPIC -g -O2 -MT source/librestclient_cpp_la-restclient.lo -MD -MP -MF source/.deps/librestclient_cpp_la-restclient.Tpo -c source/restclient.cc  -fPIC -DPIC -o source/.libs/librestclient_cpp_la-restclient.o
libtool: compile:  g++ -DHAVE_CONFIG_H -I. -Iinclude -fPIC -g -O2 -MT source/librestclient_cpp_la-restclient.lo -MD -MP -MF source/.deps/librestclient_cpp_la-restclient.Tpo -c source/restclient.cc -o source/librestclient_cpp_la-restclient.o >/dev/null 2>&1
mv -f source/.deps/librestclient_cpp_la-restclient.Tpo source/.deps/librestclient_cpp_la-restclient.Plo
/bin/bash ./libtool  --tag=CXX   --mode=compile g++ -DHAVE_CONFIG_H -I.   -Iinclude -fPIC -g -O2 -MT source/librestclient_cpp_la-connection.lo -MD -MP -MF source/.deps/librestclient_cpp_la-connection.Tpo -c -o source/librestclient_cpp_la-connection.lo `test -f 'source/connection.cc' || echo './'`source/connection.cc
libtool: compile:  g++ -DHAVE_CONFIG_H -I. -Iinclude -fPIC -g -O2 -MT source/librestclient_cpp_la-connection.lo -MD -MP -MF source/.deps/librestclient_cpp_la-connection.Tpo -c source/connection.cc  -fPIC -DPIC -o source/.libs/librestclient_cpp_la-connection.o
libtool: compile:  g++ -DHAVE_CONFIG_H -I. -Iinclude -fPIC -g -O2 -MT source/librestclient_cpp_la-connection.lo -MD -MP -MF source/.deps/librestclient_cpp_la-connection.Tpo -c source/connection.cc -o source/librestclient_cpp_la-connection.o >/dev/null 2>&1
mv -f source/.deps/librestclient_cpp_la-connection.Tpo source/.deps/librestclient_cpp_la-connection.Plo
/bin/bash ./libtool  --tag=CXX   --mode=compile g++ -DHAVE_CONFIG_H -I.   -Iinclude -fPIC -g -O2 -MT source/librestclient_cpp_la-helpers.lo -MD -MP -MF source/.deps/librestclient_cpp_la-helpers.Tpo -c -o source/librestclient_cpp_la-helpers.lo `test -f 'source/helpers.cc' || echo './'`source/helpers.cc
libtool: compile:  g++ -DHAVE_CONFIG_H -I. -Iinclude -fPIC -g -O2 -MT source/librestclient_cpp_la-helpers.lo -MD -MP -MF source/.deps/librestclient_cpp_la-helpers.Tpo -c source/helpers.cc  -fPIC -DPIC -o source/.libs/librestclient_cpp_la-helpers.o
libtool: compile:  g++ -DHAVE_CONFIG_H -I. -Iinclude -fPIC -g -O2 -MT source/librestclient_cpp_la-helpers.lo -MD -MP -MF source/.deps/librestclient_cpp_la-helpers.Tpo -c source/helpers.cc -o source/librestclient_cpp_la-helpers.o >/dev/null 2>&1
mv -f source/.deps/librestclient_cpp_la-helpers.Tpo source/.deps/librestclient_cpp_la-helpers.Plo
/bin/bash ./libtool  --tag=CXX   --mode=link g++ -fPIC -g -O2 -version-info 2:1:1  -o librestclient-cpp.la -rpath /usr/local/lib source/librestclient_cpp_la-restclient.lo source/librestclient_cpp_la-connection.lo source/librestclient_cpp_la-helpers.lo  -lcurl 
libtool: link: rm -fr  .libs/librestclient-cpp.a .libs/librestclient-cpp.la .libs/librestclient-cpp.lai .libs/librestclient-cpp.so .libs/librestclient-cpp.so.1 .libs/librestclient-cpp.so.1.1.1
libtool: link: g++  -fPIC -DPIC -shared -nostdlib /usr/lib/gcc/x86_64-linux-gnu/4.8/../../../x86_64-linux-gnu/crti.o /usr/lib/gcc/x86_64-linux-gnu/4.8/crtbeginS.o  source/.libs/librestclient_cpp_la-restclient.o source/.libs/librestclient_cpp_la-connection.o source/.libs/librestclient_cpp_la-helpers.o   /usr/lib/x86_64-linux-gnu/libcurl.so -L/usr/lib/gcc/x86_64-linux-gnu/4.8 -L/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../x86_64-linux-gnu -L/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../../lib -L/lib/x86_64-linux-gnu -L/lib/../lib -L/usr/lib/x86_64-linux-gnu -L/usr/lib/../lib -L/usr/lib/gcc/x86_64-linux-gnu/4.8/../../.. -lstdc++ -lm -lc -lgcc_s /usr/lib/gcc/x86_64-linux-gnu/4.8/crtendS.o /usr/lib/gcc/x86_64-linux-gnu/4.8/../../../x86_64-linux-gnu/crtn.o  -O2   -pthread -Wl,-soname -Wl,librestclient-cpp.so.1 -o .libs/librestclient-cpp.so.1.1.1
libtool: link: (cd ".libs" && rm -f "librestclient-cpp.so.1" && ln -s "librestclient-cpp.so.1.1.1" "librestclient-cpp.so.1")
libtool: link: (cd ".libs" && rm -f "librestclient-cpp.so" && ln -s "librestclient-cpp.so.1.1.1" "librestclient-cpp.so")
libtool: link: ar cru .libs/librestclient-cpp.a  source/librestclient_cpp_la-restclient.o source/librestclient_cpp_la-connection.o source/librestclient_cpp_la-helpers.o
libtool: link: ranlib .libs/librestclient-cpp.a
libtool: link: ( cd ".libs" && rm -f "librestclient-cpp.la" && ln -s "../librestclient-cpp.la" "librestclient-cpp.la" )
make[2]: Entering directory `/home/moocos/HappyCpp/Network/restclient-cpp'
m4 -I m4 -DM4_RESTCLIENT_VERSION=0.4.4-5-g01d5679 version.h.m4 > include/restclient-cpp/version.h
/bin/bash ./libtool  --tag=CXX   --mode=compile g++ -DHAVE_CONFIG_H -I.   -Iinclude -fPIC -g -O2 -MT source/librestclient_cpp_la-restclient.lo -MD -MP -MF source/.deps/librestclient_cpp_la-restclient.Tpo -c -o source/librestclient_cpp_la-restclient.lo `test -f 'source/restclient.cc' || echo './'`source/restclient.cc
libtool: compile:  g++ -DHAVE_CONFIG_H -I. -Iinclude -fPIC -g -O2 -MT source/librestclient_cpp_la-restclient.lo -MD -MP -MF source/.deps/librestclient_cpp_la-restclient.Tpo -c source/restclient.cc  -fPIC -DPIC -o source/.libs/librestclient_cpp_la-restclient.o
libtool: compile:  g++ -DHAVE_CONFIG_H -I. -Iinclude -fPIC -g -O2 -MT source/librestclient_cpp_la-restclient.lo -MD -MP -MF source/.deps/librestclient_cpp_la-restclient.Tpo -c source/restclient.cc -o source/librestclient_cpp_la-restclient.o >/dev/null 2>&1
mv -f source/.deps/librestclient_cpp_la-restclient.Tpo source/.deps/librestclient_cpp_la-restclient.Plo
/bin/bash ./libtool  --tag=CXX   --mode=compile g++ -DHAVE_CONFIG_H -I.   -Iinclude -fPIC -g -O2 -MT source/librestclient_cpp_la-connection.lo -MD -MP -MF source/.deps/librestclient_cpp_la-connection.Tpo -c -o source/librestclient_cpp_la-connection.lo `test -f 'source/connection.cc' || echo './'`source/connection.cc
libtool: compile:  g++ -DHAVE_CONFIG_H -I. -Iinclude -fPIC -g -O2 -MT source/librestclient_cpp_la-connection.lo -MD -MP -MF source/.deps/librestclient_cpp_la-connection.Tpo -c source/connection.cc  -fPIC -DPIC -o source/.libs/librestclient_cpp_la-connection.o
libtool: compile:  g++ -DHAVE_CONFIG_H -I. -Iinclude -fPIC -g -O2 -MT source/librestclient_cpp_la-connection.lo -MD -MP -MF source/.deps/librestclient_cpp_la-connection.Tpo -c source/connection.cc -o source/librestclient_cpp_la-connection.o >/dev/null 2>&1
mv -f source/.deps/librestclient_cpp_la-connection.Tpo source/.deps/librestclient_cpp_la-connection.Plo
/bin/bash ./libtool  --tag=CXX   --mode=compile g++ -DHAVE_CONFIG_H -I.   -Iinclude -fPIC -g -O2 -MT source/librestclient_cpp_la-helpers.lo -MD -MP -MF source/.deps/librestclient_cpp_la-helpers.Tpo -c -o source/librestclient_cpp_la-helpers.lo `test -f 'source/helpers.cc' || echo './'`source/helpers.cc
libtool: compile:  g++ -DHAVE_CONFIG_H -I. -Iinclude -fPIC -g -O2 -MT source/librestclient_cpp_la-helpers.lo -MD -MP -MF source/.deps/librestclient_cpp_la-helpers.Tpo -c source/helpers.cc  -fPIC -DPIC -o source/.libs/librestclient_cpp_la-helpers.o
libtool: compile:  g++ -DHAVE_CONFIG_H -I. -Iinclude -fPIC -g -O2 -MT source/librestclient_cpp_la-helpers.lo -MD -MP -MF source/.deps/librestclient_cpp_la-helpers.Tpo -c source/helpers.cc -o source/librestclient_cpp_la-helpers.o >/dev/null 2>&1
mv -f source/.deps/librestclient_cpp_la-helpers.Tpo source/.deps/librestclient_cpp_la-helpers.Plo
/bin/bash ./libtool  --tag=CXX   --mode=link g++ -fPIC -g -O2 -version-info 2:1:1  -o librestclient-cpp.la -rpath /usr/local/lib source/librestclient_cpp_la-restclient.lo source/librestclient_cpp_la-connection.lo source/librestclient_cpp_la-helpers.lo  -lcurl 
libtool: link: rm -fr  .libs/librestclient-cpp.a .libs/librestclient-cpp.la .libs/librestclient-cpp.lai .libs/librestclient-cpp.so .libs/librestclient-cpp.so.1 .libs/librestclient-cpp.so.1.1.1
libtool: link: g++  -fPIC -DPIC -shared -nostdlib /usr/lib/gcc/x86_64-linux-gnu/4.8/../../../x86_64-linux-gnu/crti.o /usr/lib/gcc/x86_64-linux-gnu/4.8/crtbeginS.o  source/.libs/librestclient_cpp_la-restclient.o source/.libs/librestclient_cpp_la-connection.o source/.libs/librestclient_cpp_la-helpers.o   /usr/lib/x86_64-linux-gnu/libcurl.so -L/usr/lib/gcc/x86_64-linux-gnu/4.8 -L/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../x86_64-linux-gnu -L/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../../lib -L/lib/x86_64-linux-gnu -L/lib/../lib -L/usr/lib/x86_64-linux-gnu -L/usr/lib/../lib -L/usr/lib/gcc/x86_64-linux-gnu/4.8/../../.. -lstdc++ -lm -lc -lgcc_s /usr/lib/gcc/x86_64-linux-gnu/4.8/crtendS.o /usr/lib/gcc/x86_64-linux-gnu/4.8/../../../x86_64-linux-gnu/crtn.o  -O2   -pthread -Wl,-soname -Wl,librestclient-cpp.so.1 -o .libs/librestclient-cpp.so.1.1.1
libtool: link: (cd ".libs" && rm -f "librestclient-cpp.so.1" && ln -s "librestclient-cpp.so.1.1.1" "librestclient-cpp.so.1")
libtool: link: (cd ".libs" && rm -f "librestclient-cpp.so" && ln -s "librestclient-cpp.so.1.1.1" "librestclient-cpp.so")
libtool: link: ar cru .libs/librestclient-cpp.a  source/librestclient_cpp_la-restclient.o source/librestclient_cpp_la-connection.o source/librestclient_cpp_la-helpers.o
libtool: link: ranlib .libs/librestclient-cpp.a
libtool: link: ( cd ".libs" && rm -f "librestclient-cpp.la" && ln -s "../librestclient-cpp.la" "librestclient-cpp.la" )
 /bin/mkdir -p '/usr/local/lib'
 /bin/bash ./libtool   --mode=install /usr/bin/install -c   librestclient-cpp.la '/usr/local/lib'
libtool: install: /usr/bin/install -c .libs/librestclient-cpp.so.1.1.1 /usr/local/lib/librestclient-cpp.so.1.1.1
libtool: install: (cd /usr/local/lib && { ln -s -f librestclient-cpp.so.1.1.1 librestclient-cpp.so.1 || { rm -f librestclient-cpp.so.1 && ln -s librestclient-cpp.so.1.1.1 librestclient-cpp.so.1; }; })
libtool: install: (cd /usr/local/lib && { ln -s -f librestclient-cpp.so.1.1.1 librestclient-cpp.so || { rm -f librestclient-cpp.so && ln -s librestclient-cpp.so.1.1.1 librestclient-cpp.so; }; })
libtool: install: /usr/bin/install -c .libs/librestclient-cpp.lai /usr/local/lib/librestclient-cpp.la
libtool: install: /usr/bin/install -c .libs/librestclient-cpp.a /usr/local/lib/librestclient-cpp.a
libtool: install: chmod 644 /usr/local/lib/librestclient-cpp.a
libtool: install: ranlib /usr/local/lib/librestclient-cpp.a
libtool: finish: PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/sbin" ldconfig -n /usr/local/lib
----------------------------------------------------------------------
Libraries have been installed in:
   /usr/local/lib

If you ever happen to want to link against installed libraries
in a given directory, LIBDIR, you must either use libtool, and
specify the full pathname of the library, or use the `-LLIBDIR'
flag during linking and do at least one of the following:
   - add LIBDIR to the `LD_LIBRARY_PATH' environment variable
     during execution
   - add LIBDIR to the `LD_RUN_PATH' environment variable
     during linking
   - use the `-Wl,-rpath -Wl,LIBDIR' linker flag
   - have your system administrator add LIBDIR to `/etc/ld.so.conf'

See any operating system documentation about shared libraries for
more information, such as the ld(1) and ld.so(8) manual pages.
----------------------------------------------------------------------
 /bin/mkdir -p '/usr/local/share/doc/restclient-cpp'
 /usr/bin/install -c -m 644 README.md '/usr/local/share/doc/restclient-cpp'
 /bin/mkdir -p '/usr/local/include/restclient-cpp'
 /usr/bin/install -c -m 644 include/restclient-cpp/restclient.h include/restclient-cpp/version.h include/restclient-cpp/connection.h include/restclient-cpp/helpers.h '/usr/local/include/restclient-cpp'
make[2]: Leaving directory `/home/moocos/HappyCpp/Network/restclient-cpp'
make[1]: Leaving directory `/home/moocos/HappyCpp/Network/restclient-cpp'
[~/HappyCpp/Network/restclient-cpp]















/**************************************************************************/

[~/HappyCpp/Network/cpr-example/build]
moocos-> ./example 
{
    "args": {},
    "headers": {
        "Accept": "*/*",
        "Host": "httpbin.org",
        "User-Agent": "curl/7.35.0"
    },
    "origin": "216.3.4.114",
    "url": "https://httpbin.org/get"
}









/**************************************************************************/

[~/HappyCpp/Network/curlcpp/build]
moocos-> make -j2
Scanning dependencies of target curlcpp
[  8%] [ 16%] Building CXX object src/CMakeFiles/curlcpp.dir/curl_easy.cpp.o
Building CXX object src/CMakeFiles/curlcpp.dir/curl_header.cpp.o
[ 25%] Building CXX object src/CMakeFiles/curlcpp.dir/curl_form.cpp.o
[ 33%] Building CXX object src/CMakeFiles/curlcpp.dir/curl_multi.cpp.o
[ 41%] Building CXX object src/CMakeFiles/curlcpp.dir/curl_share.cpp.o
[ 50%] Building CXX object src/CMakeFiles/curlcpp.dir/curl_info.cpp.o
[ 58%] Building CXX object src/CMakeFiles/curlcpp.dir/curl_cookie.cpp.o
[ 66%] Building CXX object src/CMakeFiles/curlcpp.dir/curl_exception.cpp.o
[ 75%] Building CXX object src/CMakeFiles/curlcpp.dir/cookie.cpp.o
[ 83%] Building CXX object src/CMakeFiles/curlcpp.dir/cookie_date.cpp.o
[ 91%] Building CXX object src/CMakeFiles/curlcpp.dir/cookie_time.cpp.o
[100%] Building CXX object src/CMakeFiles/curlcpp.dir/cookie_datetime.cpp.o
Linking CXX static library libcurlcpp.a
[100%] Built target curlcpp

```
