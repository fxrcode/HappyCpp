#include <iostream>
#include <memory>
#include <mutex>
#include <vector>
#include <thread>

//  https://baptiste-wicht.com/posts/2012/03/cpp11-concurrency-part1-start-threads.html
//  this is the 1st thread from this blog

std::mutex mutex;

void hello(){
  // std::lock_guard<std::mutex> guard(mutex);
  std::cout << "Hello from thread " << std::this_thread::get_id() << std::endl;
}

int main()
{
	std::vector<std::thread> threads;

	for (int i = 0; i < 5; ++i)
	{
    threads.push_back(std::thread(hello));
    #if 1
		threads.push_back(std::thread([]() {
			std::cout << "Hello from thread " << std::this_thread::get_id() << std::endl;
    }));
    #endif
	}

	for (auto &thread : threads)
	{
		thread.join();
	}

	return 0;
}

/************************************************
paxos  ~/Study/HappyCpp/DataStruct/embedded-resources/examples/cpp   master ●  ./prog
Hello from thread Hello from thread 140622452832000
140622461224704
Hello from thread 140622427653888
Hello from thread 140622436046592
Hello from thread 140622444439296
 **************************************************/