#include <iostream>
#include <memory>
#include <mutex>
#include <vector>
#include <thread>
#include <chrono>

//  https://baptiste-wicht.com/posts/2012/03/cp11-concurrency-tutorial-part-2-protect-shared-data.html
//  this is the 2nd thread from this blog

class Counter
{
public:
	int value;

	Counter() : value(0) {}

	void increment()
	{
		++value;
	}
};

void test()
{
	Counter counter;

	std::vector<std::thread> threads;
	for (int i = 0; i < 500; ++i)
	{
		threads.push_back(std::thread([&counter]() {
			for (int i = 0; i < 100; ++i)
			{
				counter.increment();
			}
		}));
	}

	for (auto &thread : threads)
	{
		thread.join();
	}

	std::cout << counter.value << std::endl;
}

int main()
{
	while (1)
	{
		test();
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}

	return 0;
}

/************************************************
 paxos  ~/Study/HappyCpp/DataStruct/embedded-resources/examples/cpp   master ●  ./prog
50000
49867
49857
49962
50000
49927
49932
49935
49816
 **************************************************/