# HappyCpp
* 07/06/2017: Added Deep learning folder, the 1st one is tinyflow.
* 06/11/2017: added PKU coursera C++, used sanitizers (Address/Thread), added LDD3 from duxing2007.
* 05/28/2017: use wayou's GBK-to-UTF to convert Deng junhui's C++ code in Datastruct/dsa/
* 01/11/2017: Cleared DistSys and added liblogcabin, Diego's Raft in C++ in 2k loc, ref by https://taozj.org/201612/learn-note-of-distributed-system-(4)-raft-consensus.html
* 12/19/2016: Added DistSys: libpaxos3, raft, etc...
* 11/18/2016: Added Network folder to learn Muduo and Boost, now boost coroutine cmake works.
* 11/16/2016: Reorganized folders to keep clean.
* 10/25/2016: Merged cppp into HappyCpp.
* 10/18/2016: Added OStep/, the book and HW codes of the BEST OS book: OSTEP
* 10/14/2016: Added tlpi-dist-0726/, 'The Linux Programming Interface' source code and compiled. Good resource to learn with APUE 3rd.
* 10/12/2016: I love duxing2007/ldd3-examples-3.x, fully compiled on kernel v3.13!
* 10/6/2016: CFAQ is the misc place to play Clang
* 10/5/2016: git apue.3e, apt-get install libbsd for make
* 9/29/2016: git yanweimin's data structure
* 3/30/2016: template of Lisp/LCTHW, combine C4 into HappyCpp

## C++
* CPPP5
* PKU coursera

## DL
* Added tinyflow, learn how to build a training platform in 2k LOC.

## DistSys
* Learn the best paxos/raft implementation
* Added COursera UIUC Cloud Computing Part I gossip project.

## OStep
* So many sync in ldd3, so I start from Concurrency. Also need to study the Virtual Memory.

## Network
* Learn Network programming with Boost, Muduo!

## LinuxProg
### tlpi
* The linux programming Interface source code. Easy to compile on Ubuntu 14.04.
* Good companion with APUE.3ed

## ldd3-examples-3.x
* I checked out from duxing2007, perfect!
* I changed to branch linux-4.9 and fully make successed on Ubuntu 16.04!

## datastructure
### datastructure_yanweimin
* this is the source code yanweimin's data structure book. used in moocos's data structure.
* eg: in `lab2/kern/mm/default_pmm.c`, it mentioned Yan Wei Min's Page 196~198 section 8.2. The implement of doubly list. 
* Deng Junhui's C++ Data struct. it contains GBK chinese. I used [batch convert GBK to UTF-8](https://wayou.github.io/2016/01/01/batch-convert-text-encoding-from-gbk-2-utf8/) to converted.

### embedded-resource
* clone from "embedded-resources" git repo
* good example of [circular-buffer in C++](https://embeddedartistry.com/blog/2017/4/6/circular-buffers-in-cc)

## APUE.3ed
* Quick review linux API.
* Master Pthreads

## CFAQ (misc learning of C)
* llnl is pthread learning from computing.llnl.gov/tutorial/pthreads

## Build Lisp
* Chap 5
	* Greenspun's 10th Rule: mpc (micro parser combinators)

## LCTHW

## C4_stepBystep

