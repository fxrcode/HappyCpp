#include <stdio.h>
#include "mythreads.h"
#include <stdlib.h>
#include <pthread.h>
#include <sched.h>
#include <unistd.h>

void *
mythread(void *arg) {
    printf("%s\n", (char *) arg);
    sleep(0.5);
    return NULL;
}

int
main(int argc, char *argv[])
{                    
    if (argc != 1) {
        fprintf(stderr, "usage: main\n");
        exit(1);
    }

    pthread_t p1, p2, p3;
    printf("main: begin\n");
    pthread_create(&p1, NULL, mythread, "A"); 
    pthread_create(&p2, NULL, mythread, "B");
    pthread_create(&p3, NULL, mythread, "C");
    // join waits for the threads to finish
    pthread_join(p1, NULL); 
    pthread_join(p2, NULL); 
    pthread_join(p3, NULL); 
    printf("main: end\n");
    return 0;
}
