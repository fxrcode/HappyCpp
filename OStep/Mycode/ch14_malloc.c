#include <stdio.h>
#include <stdlib.h>

void f(int iter) {
  printf("Calling f() for %d time\n", (int)iter);
  int *x = (int *)malloc(sizeof(int) * 10);
  printf("This size of pointer x is %d\n", sizeof(x));

  int y[10];
  printf("The size of y array is %d\n", sizeof(y));
}

int main() {
  f(1);
  printf("Hello world\n");
  f(2);
   return 0;
}
