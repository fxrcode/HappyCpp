/**
 * https://stackoverflow.com/questions/35711146/function-allocates-always-to-the-same-address-and-the-variable-doesnt-change
 * Dr. Liu asked in GPU, does same function in diff process can allocate same address?
 */

#include <stdio.h>
#include <stdlib.h>

int *f(int a) {
    int *tp = malloc(sizeof *p);
    if (p != NULL)
        *p = 2 * a;
    return p;
}

int main(void) {
    int *p4 = f(4);
    int *p8 = f(8);

    if (p4 != NULL && p8 != NULL) {
        printf("p4: %i / p8: %i\n", *p4, *p8);
    }
    free(p4);
    free(p8);
    return 0;
}