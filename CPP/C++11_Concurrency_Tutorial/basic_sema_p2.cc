#include <iostream>
#include <memory>
#include <mutex>
#include <vector>
#include <thread>
#include <chrono>

//  https://baptiste-wicht.com/posts/2012/03/cp11-concurrency-tutorial-part-2-protect-shared-data.html
//  this is the 2nd thread from this blog

class Counter
{
public:
	int value;

	Counter() : value(0) {}

	void increment()
	{
		++value;
	}

	void decrement(){
		if(value == 0){
				throw "Value cannot be less than 0";
		}

		--value;
}
};

class ConcurrentCounter
{
public:
	std::mutex mutex;
	Counter counter;

	void increment()
	{
		mutex.lock();
		counter.increment();
		mutex.unlock();
	}

	void decrement_bug()
	{
		mutex.lock();
		counter.decrement();
		mutex.unlock();
	}

	void decrement() {
		mutex.lock();
		try {
			counter.decrement();
		} catch (std::string e) {
			mutex.unlock();
			throw e;
		}
		mutex.unlock();
	}

	int value() {
		return counter.value;
	}
};

class ConcurrentSafeCounter {
public:
	std::mutex mutex;
	Counter counter;

	void increment(){
			std::lock_guard<std::mutex> guard(mutex);
			counter.increment();
	}

	void decrement(){
			std::lock_guard<std::mutex> guard(mutex);
			counter.decrement();
	}
	int value() {
		return counter.value;
	}
};

void test()
{
	ConcurrentSafeCounter counter;

	std::vector<std::thread> threads;
	for (int i = 0; i < 500; ++i)
	{
		threads.push_back(std::thread([&counter]() {
			for (int i = 0; i < 100; ++i)
			{
				if (i == 3 || i == 50)
					counter.increment();
				if (i == 0 || i == 10 || i == 20)
					counter.decrement();
			}
		}));
	}

	for (auto &thread : threads)
	{
		thread.join();
	}

	std::cout << counter.value() << std::endl;
}

int main()
{

	test();

#if 0
	while (1)
	{
		test();
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}
#endif

	return 0;
}

/************************************************
 paxos  ~/Study/HappyCpp/DataStruct/embedded-resources/examples/cpp   master ●  ./prog
50000
49867
49857
49962
50000
49927
49932
49935
49816
 **************************************************/