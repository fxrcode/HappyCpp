#include <algorithm>
#include <numeric>
using std::accumulate;

#include <iterator>
using std::begin; using std::end;

#include <vector>
using std::vector;

#include <list>
using std::list;

#include <string>
using std::string;

#include <iostream>
using std::cout; using std::endl;

int main() {
    vector<int> ivec = {27, 210, 12, 47, 109, 83};
    auto sum = accumulate(ivec.cbegin(), ivec.cend(), 0);
    cout << "sum all ints in vec: " << sum << endl;


    vector<string> svec = {"val1", "val2", "val3"};
    string scnt = accumulate(svec.cbegin(), svec.cend(), string(""));
    cout << "sum of strings in vec: " << scnt << endl;
    return 0;
}