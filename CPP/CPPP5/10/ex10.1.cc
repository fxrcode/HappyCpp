#include <algorithm>
using std::find;
using std::count;

#include <iterator>
using std::begin; using std::end;

#include <vector>
using std::vector;

#include <list>
using std::list;

#include <string>
using std::string;

#include <iostream>
using std::cout; using std::endl;

int main() {
    vector<int> ivec = {27, 210, 12, 47, 109, 83};
    auto icnt = count(ivec.cbegin(), ivec.cend(), 47);
    cout << "count of 47 in vec: " << icnt << endl;


    vector<string> svec = {"val1", "val2", "val3"};
    auto scnt = count(svec.cbegin(), svec.cend(), "val3");
    cout << "count of 'val3' in vec: " << scnt << endl;
    return 0;
}