#include <algorithm>
using std::sort;
using std::for_each;
using std::partition;

#include <functional>
using std::bind;
using namespace std::placeholders;

#include <string>
using std::string;

#include <vector>
using std::vector;

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

bool is5more(const string &s)
{
    return s.size() > 4 ? true : false;
}

int main()
{
    vector<string> svec = {"a", "as", "aasss", "aaaaassaa", "aaaaaabba", "aaa"};
    auto end_partition = partition(svec.begin(), svec.end(), is5more);
    svec.erase(end_partition, svec.end());
    for (const string &s : svec) 
        cout << s << " ";
    cout << endl;

    return 0;
}