#include <algorithm>
using std::find; using std::find_if; using std::for_each;
using std::remove_copy_if; using std::replace_copy;
using std::reverse; 

#include <iterator>
using std::back_inserter;

#include <vector>
using std::vector;

#include <iostream>
using std::cout; using std::endl;

int main() {
    vector<int> ilist = {0,1,2,3,4,5,6,7,0,8,9,0};
    vector<int> ivec;
    replace_copy(ilist.cbegin(), ilist.cend(),
                back_inserter(ivec), 0, 42);
    for (auto i : ivec)
        cout << i << " ";

    cout << endl;
    return 0;
}