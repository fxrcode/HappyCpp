#include <iostream>
#include "Sales_item.h"

int main()
{
    Sales_item i1, i2;
    std::cin >> i1 >> i2;
    if (i1.isbn() == i2.isbn()) {
        std::cout << i1 + i2 << std::endl;
        return 0;
    }
    else {
        std::cout << "I request 2 sales_item with same ISBN!!!" << std::endl;
        return 0;
    }
}
