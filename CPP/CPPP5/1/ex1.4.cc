#include <iostream>

int main()
{
    std::cout << "Enter 2 real numbers for multi" << std::endl;
    double v1 = 0.0f, v2 = 0.0f, v3 = 0.0f;
    std::cin >> v1 >> v2;
    std::cout << "The product of " << v1 << " and " << v2 << " is " << v1 * v2 << std::endl;
    return 0;
}
