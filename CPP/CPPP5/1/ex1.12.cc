#include <iostream>

int main()
{
    int sum = 0;
    for (int i = -100; i <= 100; ++i) {
        sum += i;
    }
    std::cout << "this for loop sum up from -100 til 100: " << sum << std::endl;
    return 0;
}
