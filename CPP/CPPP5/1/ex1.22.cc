#include <iostream>
#include "Sales_item.h"

int main()
{
    // ttt: A good implementation, solve the init of ping-pong buffer problem,
    //      reminds me the fflink's bounding_DMA
    Sales_item total;
    if (std::cin >> total) {
        Sales_item temp;
        while (std::cin >> temp) {
            if (temp.isbn() == total.isbn()) {
                total += temp;
            }
            else {
                std::cout << temp << "Oh? not the same isbn as the first item! " << std::endl;
            }
        }
        std::cout << total << std::endl;
    }
    else {
        // warning: trigraph ??! converted to | [-Wtrigraphs]
        //      std::cerr << "No data???!" << std::endl;
        std::cerr << "No data?!" << std::endl;
        return -1;
    }
    return 0;
}
