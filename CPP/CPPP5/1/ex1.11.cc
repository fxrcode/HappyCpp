#include <iostream>

/**
 * return error code, like node.js
 */
int helper(int l, int r) 
{
    if (l > r)  return -1;
    while (l <= r) {
        std::cout << l << " ";
        l++;
    }   
    std::cout << std::endl;  // flushing the buffer 
    return 0;
}

int main()
{
    int left = 0, right = -1;
    std::cout << "Give me a range, then I will print all numbers in the range" << std::endl;
    std::cin >> left >> right;
    int err = helper(left, right);
    if (err != 0)  return -1;
    return 0;
}
