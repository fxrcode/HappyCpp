/*
 * based on the add_item, use iostream and Sales_item.h, along with File redirection
 * to print all the items info on the std output
 */

#include <iostream>
#include <vector>
#include "Sales_item.h"

using namespace std;

void firstTry()
{
    vector<Sales_item> v;
    Sales_item item;
    while (cin >> item) {
        v.push_back(item);
    }

    for (Sales_item i : v) {
        cout << i << "\n";
    }
    cout << "\nSize: " << v.size() << "\n";
}

/**
 * @brief pezySol
 * https://github.com/pezy/CppPrimer/blob/master/ch01/ex1_20.cpp
 */
void pezySol()
{
    Sales_item temp;
    while (cin >> temp) {
        cout << temp << "\n";
    }
    cout << endl;
}

int main()
{
    firstTry();
//    pezySol();
}
