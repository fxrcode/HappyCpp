/**
 * ex1.23 ---> assumption: same isbn will not show again later, or, I have to use map for records
 * Mimic ex1.22, notice the cnt before while loop
 * 
 * Tips: To run, use "./ex1.23.exe < data/countTransactions > data/countTransactionsResult"
 */

#include <iostream>
#include "Sales_item.h"

int main(int argc, char *argv[])
{

	Sales_item ping, pong;
	if (std::cin >> ping) {
		int cnt = 1;
		while (std::cin >> pong) {
			if (ping.isbn() == pong.isbn()) {
				++cnt;
			}
			else {
				std::cout << ping << " occurs " << cnt << " times " << std::endl;
				ping = pong;
				cnt = 1;
			}
		}

		std::cout << ping << " occurs " << cnt << " times " << std::endl;
	}

	return 0;
}