#include <iostream>

void rewrite2() 
{
    for (int v = 10; v >= 0; v--) {
        std::cout << v << " ";
    }
    std::cout << std::endl;
    //return 0;
}

int rewrite1()
{
    int sum = 0;
    for (int i = 50; i <= 100; ++i) {
        sum += i;
    }
    return sum;
}

int main()
{
   int ans = rewrite1();
   std::cout << ans << std::endl;
   rewrite2();
   return 0; 
}
