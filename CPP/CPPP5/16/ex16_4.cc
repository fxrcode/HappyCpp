/*
===============================================================================
C++ Primer 5th Exercise Answer Source Code
Q: template works as find algs.
===============================================================================
*/

#include <iostream>
#include <string>
#include <vector>
#include <list>

using namespace std;

template <typename InputIt, typename T>
InputIt findfx(const InputIt begin, const InputIt end, const T& needle) {
    for (auto it = begin; it != end; ++it) {
        if (*it == needle) {
            return it;
        }
    }
    return end;
}

/**
 * testing - Auto Comment Blocks
 */
int main() {
    list<int> li{1,2,3,4,5,6,7,8,9,10};

    int needle = 42;
    auto ret = findfx(li.cbegin(), li.cend(), needle);

    // https://stackoverflow.com/questions/9661952/why-in-conditional-operator-second-and-third-operands-must-have-the-same-t
    cout << "The value " << needle
        << (ret == li.cend()
             ? ". I didn't find it" : " is present") << endl;

    
    vector<string> vs{"ni", "hao", "ma", "fxr", "code", "42"};
    string val = "fxr";
    auto ans = findfx(vs.cbegin(), vs.cend(), val);

    cout << "The string " << val
        << (ans == vs.cend()
             ? ". I didn't find it" : " is present") << endl;
    return 0;
}
