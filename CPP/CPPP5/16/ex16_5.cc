/*
===============================================================================
C++ Primer 5th Exercise Answer Source Code
Q: template to print array, Use nontype template parameters
===============================================================================
*/

#include <iostream>

/**
 * Notice: N is unsigned, not typename!
 */
template <typename T, unsigned N> 
void fxprint(const T (&arr)[N]) {
    for (auto i = 0; i != N; ++i) {
        std::cout << arr[i] << std::endl;
    }
}

int main() {
    fxprint("nihao ma");
    return 0;
}