#include <iostream>
#include <numeric>

using namespace std;

template <typename T>
int compare(const T &v1, const T &v2) {
    if (less<T>()(v1, v2))  return -1;
    if (less<T>()(v2, v1))  return 1;
    return 0;
}

int main() {
    int v1, v2;
    cin >> v1 >> v2;
    int ret1 = compare(v1, v2);
    cout << "ret1 of v1 and v2: " << v1 << ", " << v2 << ": " << ret1 << endl;
    
    return 0;
}