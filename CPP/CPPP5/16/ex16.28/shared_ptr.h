/*
=================================================================================
C++ Primer 5th Exercise Answer Source Code
ex16.28 - Write your own version of shared_ptr and unique_ptr to learn
    function/class template and efficiency of chap 16.1

This is the own version of shared_ptr
=================================================================================
*/

#pragma once
#include <functional>
#include "DebugDelete.h"

using std::cout; using std::endl;

namespace cp5 
{
    // forward declarations for friends
    template <typename> class fxr_shared_ptr;   // declare of class template for friend
    
    template <typename T>
    void swap(fxr_shared_ptr<T> &lhs, fxr_shared_ptr<T> &rhs);

    template <typename T>
    fxr_shared_ptr<T> make_shared(T val) {
        T* valp = new T{val};
        fxr_shared_ptr<T> fsp = fxr_shared_ptr<T>(valp);
        return fsp;
    }

    /**
     * @brief   homemade shared_ptr version
     */
    template <typename T>
    class fxr_shared_ptr
    {
        friend void swap<T>(fxr_shared_ptr<T>& lhs, fxr_shared_ptr<T>& rhs);
        friend fxr_shared_ptr<T> make_shared(T val) {
            fxr_shared_ptr fsp{val};
            return fsp;
        }
    public:
        // default ctor
        fxr_shared_ptr() = default;

        // ctor converse raw pointer
        explicit fxr_shared_ptr(T* p)
            : rp(p), ref_cnt{ new std::size_t(1) }, deleter{ cp5::DebugDelete() }
        { }

        // Copy Ctor
        fxr_shared_ptr(fxr_shared_ptr const &other) 
            : rp(other.rp), ref_cnt(other.ref_cnt), deleter{ other.deleter }
        {
            ++*ref_cnt;
        }

        // copy assignment
        fxr_shared_ptr& operator=(fxr_shared_ptr const &rhs) {
            // increment the ref_cnt that rhs points to
            ++*(rhs.ref_cnt);
            // decrease the ref_cnt that lhs points to
            decrement_and_destroy();
            rp = rhs.rp, ref_cnt = rhs.ref_cnt, deleter = rhs.deleter;
            return *this;
        }

        // move assignement
        fxr_shared_ptr& operator=(fxr_shared_ptr&& rhs) noexcept
        {
            swap(*this, rhs);
            rhs.decrement_and_destroy();
            return *this;
        }

        // dereference 
        T& operator*() const
        {
            return *rp;
        }

        // member access ->
        T* operator->() const
        {
            return &*rp;
        }

        // get operation
        auto get() const
        {
            return rp;
        }

        // member swap operation
        auto swap(fxr_shared_ptr &rhs) 
        {
            cp5::swap(*this, rhs);
        }

        // unique operation
        auto unique() const 
        {
            return 1 == *ref_cnt;
        }

        // use_count operation
        auto use_count() const 
        {
            return *ref_cnt;
        }
        
        // reset
        auto reset() 
        {
            decrement_and_destroy();
        }

        // reset with new raw pointer
        auto reset(T* newrp) 
        {
            if (newrp != rp) {
                decrement_and_destroy();
                // no need to new a fxr_shared_ptr
                rp = newrp;
                ref_cnt = new std::size_t(1);
            }
        }

        // reset with new raw pointer and deleter
        auto reset(T* newrp, const std::function<void(T*)> D) 
        {
            reset(newrp);
            deleter = D;
        }

        // Dtor
        ~fxr_shared_ptr() 
        {
            decrement_and_destroy();
        }
    
    private:
        T* rp;  // raw pointer
        std::size_t* ref_cnt;    // reference count, save the pointer to it
        std::function<void(T*)> deleter;    // functional type, runtime link

        // call to decrement the ref_cnt and destroy when ref_cnt is zero
        auto decrement_and_destroy() 
        {
            if (rp && 0 == --*ref_cnt) {
                delete ref_cnt;
                deleter(rp);
            }
            else if (!rp) 
                delete ref_cnt;
        
            // santity init pointer type to nullptr instead of random value
            ref_cnt = nullptr;
            rp = nullptr;
        }
    };


    /**
     *  @brief swap and big 5
     */
    template <typename T>
    inline void
    swap(fxr_shared_ptr<T>& lhs, fxr_shared_ptr<T>& rhs)
    {
        cout << "calling friend swap!" << endl;
        using std::swap;
        swap(lhs.rp, rhs.rp);
        swap(lhs.ref_cnt, rhs.ref_cnt);
        swap(lhs.deleter, rhs.deleter);
    }

    // template <typename T>
    // inline fxr_shared_ptr<T> make_shared(T val) {
    //     // difference between make_shared() and shared_ptr()
    //     // https://stackoverflow.com/questions/20895648/difference-in-make-shared-and-normal-shared-ptr-in-c
    //     T* valp = new T{val};
    //     fxr_shared_ptr<T> fsp = fxr_shared_ptr<T>(valp);
    //     return fsp;
    // }
}