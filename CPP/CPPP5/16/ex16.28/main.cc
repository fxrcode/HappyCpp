/*
=================================================================================
C++ Primer 5th Exercise Answer Source Code
ex16.28 - Write your own version of shared_ptr and unique_ptr to learn
    function/class template and efficiency of chap 16.1

This is the unit test main function
=================================================================================
*/

#include <iostream>
#include <string>
#include <vector>
#include "shared_ptr.h"
#include "unique_ptr.h"

using std::vector;
using std::cout; 
using std::endl;
using std::string;

int main() {
    #if 1
    cout << "EX16.28 - design your own shared_ptr" << endl;
    auto foo = cp5::fxr_shared_ptr<int>{ new int(42) };
    cout << *foo << endl;
    cout << foo.use_count() << endl;

    // auto ofo = cp5::fxr_shared_ptr<int>{ new int(11) };
    // cout << *ofo << endl;
    // cout << ofo.use_count() << endl;
    
    // cp5::swap(foo, ofo);
    // cout << *foo << " - " << *ofo << endl;

    string aaa = "fxrcode";
    auto bar = cp5::make_shared( 999 );
    cout << *bar << endl;
    cout << bar.use_count() << endl;
    #endif

    #if 0
    cout << "EX16.28 - design your own unique_ptr" << endl;
    auto foo = cp5::fxr_unique_ptr<int>{ new int(42) };
    cout << *foo << endl;

    vector<cp5::fxr_unique_ptr<int>> vup;
    for (int i = 0; i != 10; ++i) {
        vup.push_back(cp5::fxr_unique_ptr<int>(new int{i}));
    }

    for (auto& sp : vup) 
        cout << *sp << "\n";
    cout << endl;
    #endif

    return 0;
}