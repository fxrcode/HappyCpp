/*
===============================================================================
C++ Primer 5th Exercise Answer Source Code
Q: constexpr template return size of array
===============================================================================
*/

#include <iostream>
#include <string>
#include <vector>
#include <list>

using namespace std;

// https://stackoverflow.com/questions/15763937/unused-parameter-in-c11
template <typename T>
void ignore(T &&) { }

// https://stackoverflow.com/questions/45097171/the-body-of-constexpr-function-not-a-return-statement
template <typename T, unsigned N>
constexpr unsigned fxsizeArray(const T (&arr)[N]) 
{
    ignore(arr);
    return N;
}

int main() {
    auto ans = fxsizeArray("nihao");
    cout << "The size of this array = " <<  ans << endl;
    return 0;
}