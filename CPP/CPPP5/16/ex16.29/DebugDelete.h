/*
=================================================================================
C++ Primer 5th Exercise Answer Source Code
ex16.28 - Write your own version of shared_ptr and unique_ptr to learn
    function/class template and efficiency of chap 16.1

This is the deleter struct
=================================================================================
*/

class DebugDelete
{
public:
    // ctor
    DebugDelete(std::ostream &s = std::cout) : os(s) { } 
    template <typename T> 
    void operator()(T* p) const 
    {
        os << "deleting ptr" << std::endl;
        delete p;
    }
private:
    std::ostream &os;
};