/*
=================================================================================
C++ Primer 5th Exercise Answer Source Code
ex16.29 - Use your own version of shared_ptr and unique_ptr in your Blob
=================================================================================
*/

#ifndef BLOB_H
#define BLOB_H

#include <vector>
#include <initializer_list>
#include "shared_ptr.h"

/**
 * Blob template class using ex16.28 - your own version of shared_ptr
 */
template <typename T> 
class Blob 
{
public:
    typedef T value_type;
    typedef typename std::vector<T>::size_type size_type;

    // Ctor
    Blob();
    Blob(std::initializer_list<T> lt);

    // Ctor with two iter
    template <typename It> Blob(It beg, It end);

    // number of elements in the blob
    size_type size() { return data->size(); }
    bool    empty() { return data->empty(); }

    // push/pop elem in the blob
    void push_back(const T& t) { data->push_back(t); }
    void push_back(T&& t) { data->push_back(std::move(t)); }
    void pop_back();

    // elem access
    T& back() const;
    T& operator[](size_type i) const;

    // debug get()
    fxr_shared_ptr<std::vector<T>>& get() { return data; }
private:
    fxr_shared_ptr<std::vector<T>> data;
    void check(size_type i, const std::string &msg) const;
};

/**
 * Starting the implementation
 */

//  default constructor
template<typename T>
Blob<T>::Blob() 
    : data(make_shared_1<std::vector<T>>()) { } // TODO: better way?
// : data(make_shared<std::vector<T>>()) { }

// Ctor with initializer_list
template <typename T> 
Blob<T>::Blob(std::initializer_list<T> lt)
    : data(make_shared<std::vector<T>>(lt)) { }  // TODO: does it work?

// check
template <typename T> 
void Blob<T>::check(size_type i, const std::string &msg) const
{
    if (i >= data->size())
        throw std::out_of_range(msg);
}

// access
template <typename T> 
T& Blob<T>::back() const
{
    // TODO: why 0?
    check(0, "back on emptyr Blob");
    return data->back();
}

// template <typename T> 
// const T& Blob<T>::back() const
// {
//     // TODO: why 0?
//     check(0, "back on emptyr Blob");
//     return data->back();
// }

// operator overload
template <typename T>
T& Blob<T>::operator[](size_type i) const
{
    check(i, "subscript out of bound");
    return (*data)[i];  // deref fxr_shared_ptr to get i-th elem
}

template <typename T>
void Blob<T>::pop_back()
{
    check(0, "Pop-back on empty Blob");
    data->pop_back();
}

#endif