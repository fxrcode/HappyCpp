/*
=================================================================================
C++ Primer 5th Exercise Answer Source Code
ex16.28 - Write your own version of shared_ptr and unique_ptr to learn
    function/class template and efficiency of chap 16.1

This is the unit test main function
=================================================================================
*/

#include <iostream>
#include <string>
#include <vector>
#include "shared_ptr.h"
#include "Blob.h"

using std::vector;
using std::cout; 
using std::endl;
using std::string;


int main() {
    #if 0
    auto bar = make_shared( 999 );
    cout << *bar << endl;
    cout << bar.use_count() << endl;
    // auto bar = make_shared_1<std::vector<int>>( );
    // cout << *bar << endl;
    cout << "Hello world" << endl;
    #endif

    #if 1
    Blob<int> blob = {42,99,-1};
    for (int i = 0; i != 10; ++i) {
        blob.push_back(i);
    }

    auto data = *(blob.get());

    for (auto& d : data) 
        cout << d << "\n";
    cout << endl;
    #endif

    return 0;
}