/*
=================================================================================
C++ Primer 5th Exercise Answer Source Code
ex16.28 - Write your own version of shared_ptr and unique_ptr to learn
    function/class template and efficiency of chap 16.1

This is the own version of shared_ptr
=================================================================================
*/

#pragma once
#include <functional>
#include "DebugDelete.h"

// forward declaration of friend function
template <typename, typename> class fxr_unique_ptr;

template <typename T, typename D>
void swap(fxr_unique_ptr<T,D> &lhs, fxr_unique_ptr<T,D> &rhs);


/**
 * @brief   std::unique like class template
 */
template <typename T, typename D = DebugDelete>
class fxr_unique_ptr
{
    friend void swap<T,D>(fxr_unique_ptr<T,D>& lhs, fxr_unique_ptr<T,D>& rhs);
public:
    // prevent copy and assignment
    fxr_unique_ptr(const fxr_unique_ptr&) = delete;
    fxr_unique_ptr& operator=(const fxr_unique_ptr&) = delete;

    // Ctor default and with val
    fxr_unique_ptr() = default;
    explicit fxr_unique_ptr(T* p) : rp(p) { }
    
    // Ctor with val and deleter
    fxr_unique_ptr(T* p, D d): rp(p), deleter(d) { }

    // operator overload
    T& operator*() const { return *rp; }
    T* operator->() const { return &this->operator *(); }   // ehmmm

    // Move Ctor
    fxr_unique_ptr(fxr_unique_ptr&& fup) noexcept
        : rp(fup.rp)  { fup.rp = nullptr; }

    // Move Assignment
    fxr_unique_ptr& operator=(fxr_unique_ptr&& rhs) noexcept;

    // release method
    T* release();

    // reset method w, w/o pointer
    void reset()    noexcept { delete(rp); rp = nullptr; }
    void reset(T* p)    noexcept { delete(rp); rp = p; }

    // Dtor
    ~fxr_unique_ptr() { delete(rp); }

private:
    T* rp;
    D deleter = D();
};

// friend: swap
template <typename T, typename D>
inline void swap(fxr_unique_ptr<T,D> &lhs, fxr_unique_ptr<T,D> &rhs) {
    using std::swap;
    swap(lhs.rp, rhs.rp);
    swap(lhs.deleter, rhs.deleter);
}

// move operator
template <typename T, typename D>
inline fxr_unique_ptr<T,D>&
fxr_unique_ptr<T,D>::operator=(fxr_unique_ptr&& rhs) noexcept
{
    // prevent self-assignment
    if (this.rp != rhs.rp) {
        deleter(rp);
        rp = nullptr;
        swap(*this, rhs);
    }
    return *this;
}

// relinquish control by returning rp and make rp point to nullptr
template <typename T, typename D>
inline T* fxr_unique_ptr<T,D>::release()
{
    T* ret = rp;
    rp = nullptr;
    return ret;
}