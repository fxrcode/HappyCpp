#include <iostream>
#include <string>
#include "Sales_data_ex.h"

void ex1_23();

int main()
{
	ex1_23();
	return 0;
}

void ex1_23()
{
	Sales_data_ex pre, next;

	std::cin >> pre.name >> pre.isbn >> pre.price >> pre.units_sold;
	double total = pre.price * pre.units_sold;
	while (std::cin >> next.name >> next.isbn >> next.price >> next.units_sold) {
		if (pre.isbn == next.isbn) {
			total += next.units_sold * next.price;
		} else {
			std::cout << "revenue for: " << pre.isbn << " is " << total << std::endl;
			total = next.units_sold * next.price;
		}
		pre.name = next.name;
		pre.isbn = next.isbn;
		pre.price = next.price;
		pre.units_sold = next.units_sold;
	}
}