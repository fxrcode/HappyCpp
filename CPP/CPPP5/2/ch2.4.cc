/**
 * ch 2.4 const Qualifier
 */

#include <iostream>
#include "ch2.4.h"

static int fcn();
extern const int bufSize = fcn();

static void ch2_4_2();
static void ch2_4_3();

int main()
{
	// const int bufSize = 512;
	// bufSize = 512;
	ch2_4_3();

	return 0;
}

static int fcn()
{
	return 1024;
}

static void ch2_4_2()
{
	std::cout << "Hello: " << bufSize << std::endl;

	int i = 42;
	int &r1 = i;
	const int &r2 = i;  // const reference May refer to nonconst obj
	r1 = 4201;
	std::cout << r1 << " " << r2 << std::endl;

	const double pi = 3.14159;
	// double *ptr = &pi;
	const double *cptr = &pi;  // const pointer May point to const obj
	// *cptr = 42;
	
	double dval = 2.71828;
	cptr = &dval;
	// *cptr = 42;
	int errNumb = 200;
	int *const curErr = &errNumb;
	const double *const pip = &pi;

	*curErr = 100;

	// *pip = 2.72;
	std::cout << *curErr << std::endl;
}

static void ch2_4_3()
{
	int i = 10;
	int *const p1 = &i;
	const int ci = 42;
	const int *p2 = &ci;
	const int *const p3 = p2;
	const int &r = ci;

	i = ci;
	p2 = p3;
	std::cout << *p2 << std::endl;
}

static void ch2_4_4()
{
	const int *p = nullptr;
	constexpr int *q = nullptr;
}