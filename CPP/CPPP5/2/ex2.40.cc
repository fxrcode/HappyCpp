#include <iostream>

struct Sales_data {
	std::string name = "Designing Data-Intensive Applications";
	std::string author = "Martin Kleppmann";
	std::string ISBN = "978-1449373320";
	int year = 1970;
	int month = 12;
	int day = 17;
	unsigned int counts = 0;
	double price = 0.0;
	double total = 0.0;
	//
};

/**
 * Unit test for my struct
 * @return [description]
 */
int main()
{
	Sales_data cppp5;
	std::cin >> cppp5.name >> cppp5.author >> cppp5.price >> cppp5.counts ;
	cppp5.ISBN = "978-6666666666";

	std::cout << cppp5.price << ", " << cppp5.name << ", " << cppp5.author << std::endl;
	cppp5.total = cppp5.counts * cppp5.price;
	std::cout << "So the total is: " << cppp5.total << std::endl;
	return 0;
}