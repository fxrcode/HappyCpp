#include <iostream>

void ex1_20();
void ex1_21();
void ex1_22();
void ex1_23();

struct Sales_data {
	std::string name = "Designing Data-Intensive Applications";
	// std::string author = "Martin Kleppmann";
	std::string isbn = "978-1449373320";
	unsigned units_sold = 0;
	double price = 0.0;
	double revenue = 0.0;
	//
};

int main()
{
	ex1_23();
	return 0;
}

void ex1_20()
{
	Sales_data b1, b2, b3;
	std::cin >> b1.name >> b1.isbn >> b1.price >> b1.units_sold;
	std::cin >> b2.name >> b2.isbn >> b2.price >> b2.units_sold;
	std::cin >> b3.name >> b3.isbn >> b3.price >> b3.units_sold;
	double total = b1.price * b1.units_sold + b2.price * b2.units_sold + b3.price * b3.units_sold;
	std::cout << "Total revenue is: " << total << std::endl;
}

void ex1_21()
{
	Sales_data b1, b2;
	std::cin >> b1.name >> b1.isbn >> b1.price >> b1.units_sold;
	std::cin >> b2.name >> b2.isbn >> b2.price >> b2.units_sold;
	double total = b1.price * b1.units_sold + b2.price * b2.units_sold;
	std::cout << "Total revenue is: " << total << std::endl;
}

void ex1_22()
{
	Sales_data item;

	std::cin >> item.name >> item.isbn >> item.price >> item.units_sold;
	double total = item.price * item.units_sold;
	while (std::cin >> item.name >> item.isbn >> item.price >> item.units_sold) {
		total += item.price * item.units_sold;
	}
	std::cout << "Total revenue is: " << total << std::endl;
}

void ex1_23()
{
	Sales_data pre, next;

	std::cin >> pre.name >> pre.isbn >> pre.price >> pre.units_sold;
	double total = pre.price * pre.units_sold;
	while (std::cin >> next.name >> next.isbn >> next.price >> next.units_sold) {
		if (pre.isbn == next.isbn) {
			total += next.units_sold * next.price;
		} else {
			std::cout << "revenue for: " << pre.isbn << " is " << total << std::endl;
			total = next.units_sold * next.price;
		}
		pre.name = next.name;
		pre.isbn = next.isbn;
		pre.price = next.price;
		pre.units_sold = next.units_sold;
	}
}