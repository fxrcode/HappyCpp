#include <iostream>

int main(int argc, char* argv[])
{
	int a = 2, b = 4;
	int &ref = a;
	ref = b;
	// http://stackoverflow.com/a/7181452/3984911
	//		so ref is not updated, just updated a!
	std::cout << ref << ", " << a << std::endl;
#if 0
	int i = 100, &r1 = i;
	double d = 2.17, &r2 = d;

	r2 = 3.14159;
	std::cout <<  d << std::endl;

	r2 = r1;
	std::cout << i << std::endl;

	// r1 = r2;
	// std::cout << i << std::endl;

	i = r2;
	std::cout << i << std::endl;

	r1 = d;
	std::cout << i << std::endl;
#endif

	return 0;
}