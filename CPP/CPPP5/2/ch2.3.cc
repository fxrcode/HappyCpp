/**
 * chap 2.3 compound type
 */

#include <iostream>

int main(int argc, char *argv[])
{
#if 0
    int ival = 1024;
    int *pi = &ival;
    int **ppi = &pi;

    std::cout << "The value of ival\n"
            << "direct value: " << ival << "\n"
            << "indirect value: :" << *pi << "\n"
            << "double indirect value: " << **ppi
            << std::endl;
#endif

	int i = 42, j = 1024;
	int *p = &j;
	int *&r = p; // right-to-left rule: r is a reference to a pointer to int.

	std::cout << *r << std::endl;
	r = &i;
	*r = 0;
	std::cout << *r << std::endl;
    return 0;
}
