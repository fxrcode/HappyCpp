/**
 * Ex 2.18: Write a program to change the value of a pointer. Write code to change the value to which the pointer points
 * 
 */
#include <iostream>

int main(int argc, char* argv[])
{
	// error: ‘void*’ is not a pointer-to-object type
	// void *ptr = nullptr;
	double *ptr = nullptr;
	double d = 32;
	ptr = &d;
	std::cout << *ptr << std::endl;

	d = 2.71828;
	std::cout << *ptr << std::endl;
	return 0;
}