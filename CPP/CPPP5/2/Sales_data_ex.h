#ifndef SALES_DATA_EX_H
#define SALES_DATA_EX_H

#include <iostream>

/**
 * sales_data_ex is the struct for ex2.42 to mimic Sales_data struct
 */
struct Sales_data_ex {
	std::string name = "Designing Data-Intensive Applications";
	std::string author = "Martin Kleppmann";
	std::string isbn = "978-1449373320";
	unsigned units_sold = 0;
	double price = 0.0;
	double revenue = 0.0;
	//
};
#endif