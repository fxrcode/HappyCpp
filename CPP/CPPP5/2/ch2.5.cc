#include <iostream>
#include "Sales_data.h"
#include "../1/Sales_item.h"

void ex2_33();

int main()
{
	typedef double wage;
	typedef wage base, *p;

	// using SD = Sales_data;
	// SD item;
	// item.bookNo = "234-8097";

	// std::cout << item.bookNo << std::endl;
	// SD item = {
	// 	.bookNo = "Ni Hao",
	// 	.units_sold = 10,
	// 	.revenue = 1000.24
	// };
	
	// typedef char *pstring;
	// const pstring cstr = 0;

	// using SI = Sales_item;
	// SI val1;
	// SI val2 = SI( "21-2323-X0" );
	// auto item = val1 + val2;
	// std::cout << val2 << std::endl;

	ex2_33();

	return 0;
}

void ex2_33()
{
	int i = 0, &r = i;
	auto a = r;
	const int ci = i, &cr = ci;
	auto b = ci;
	auto c = cr;
	auto d = &i;
	auto e = &ci;

	const auto f = ci;
	auto &g = ci;
	auto &h = 42;
	const auto &j = 42;
	auto k = ci, &l = i;
	auto &m = ci, *p = &ci;
	auto &n = i, *p2 = &ci;

	a = 42;
	b = 42;
	c = 42;
	d = 42;
	e = 42;
	g = 42;
}