#include <iostream>

int main(int argc, char* argv[])
{
	// int i = -1, &r = -42;
	const int i = -1, &r = -42;
	std::cout << i << " " << r << std::endl;
	return 0;
}