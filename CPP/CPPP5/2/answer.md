# Answer

## ex 2.5
* as mentioned in p40: specifying the Type of a Literal, this is easily understood:
    * 'a' is a char, L'a' has override it to wchar_t, "a" = 'a'+'\0': array of char, L"a" = array of wchar_t: 'a', '\0'.
    * 10 is int, 10u is unsigned int, 10uL is unsigned long, 012 is octal, 0xC is hex.
    * 3.14 is double, 3.14f is float, 3.14L is long double
    * 10 is int, 10u is unsigned int, 10. is double, 10e-2 is float.

## ex 2.6
* first one is normal int in decimal, the second is int in octal

## ex 2.7
* as mentioned in P39, '\' followed by 1-3 octal digits, so I checked ascii table, oct: 145 is 'e', '012' is a new line. so the answer is: "Who goes with Fergus?(new line)"
* 3.14 x 10 = 31.4 long double
* 1024.0 floating
* 3.14 long double

## ex 2.9
* need to declare input_value ahead of cin.
* narrowing double to int, so i will only be 3.
* note that, wage need to be declare first, also assignment operator will return the right value, so salary will be 9999.99 after I fix the wage declaration issue.
* assign double to int will narrow the value, lead to i = 3.

## ex 2.10
* variable outside of function is init to empty string or 0,
* variable inside of function is not init, so it has undefined value. BUT std::string has a value that is defined by the std class, so it's empty string.

## ex 2.11
* this is definition
* this is definition
* this is declaration

## ex 2.12
* narrow double literal to int var
* yes
* no
* no, can't start from number
* yes

## ex 2.13
* j = 100

## ex 2.14
* 100,45

## ex 2.15
* ival = 1, narrow double to int
* invalid, reference must be init to object
* valid, rval2 is a reference to object ival
* invalid, reference is not init.

## ex 2.16
* valid, update the object that r2 refer to 3.14159. ~~r2 is a reference, can't be rebind~~
* valid, automatic convert. ~~invalid~~
* valid, just truncate. ~~i is int, r2 is reference~~
* valid

## ex 2.17
* should be 10 10

## ex 2.18
* as void* says: we can't use a void* to operate on the object it addresses, we don't know the type, so no operation.

## ex 2.19
* reference is not an object, just alias name of the referee.
* pointer hold the address, and pointer is an object.

## ex 2.20
* p1 is the address of i,
* *p1 updated to 42^2

## ex 2.21
* pointer's type must match object's. ~~dp points to i, although it expects double~~
* pointer's type must match object's. ~~ip points to 0 which is nullptr~~
* p points to 0's address.

## ex 2.22
* if p is not nullptr, then true
* if p points to an un-zero int, then true.

## ex 2.23
* NO! ~~Sure, check if p == 0~~

## ex 2.24
* p points to the address holds 42.
* because pointer's type should match the object's! ~~lp illeagle?~~

## ex 2.25
* ip is pointer to int, i is int, r is reference to int i
* i is int, ip is pointer to int 0, so it's nullptr.
* ip is pointer to int, ip2 is int.

## ex 2.26
* illegal, since const needs to be init.
* legal
* legal
* illegal to update read-only object(sz)

## ex 2.27
* illegal, r must refer to object
* legal
* GOOD one, const is also for &r, so according to P61 exception, can init to any expression, so legal! ~illegal, r must refer to obj~
* legal
* legal
* illegal, reference has no high-level const
* legal

## ex 2.28
* illegal, cp must init.~~legal~~
* illegal, p2 must init.~~legal~~
* illegal, ic must init.~~legal~~
* illegal, p3 must init.~~legal~~
* legal

## ex 2.29
* illegal, ic is not initialized
* illegal, normal pointer can't be assigned to const pointer.
* illegal, can't assign const* to normal pointer
* illegal, can't assign const* to normal pointer
* illegal, p2 is const pointer
* illegal, ic is const int

## ex 2.30
* v2 is low
* p2 is low, p3 is high-low, r2 is low

## ex 2.31
* legal
* illegal, const to non-const
* legal
* illegal
* legal

## ex 2.32
* illegal, convert int to int*.
* fix by passing the address of null: *p = &null.

## ex 2.33
* TODO

## ex 2.35
* i is const int.
* j is int.
* k is const int&
* p is const int*.
* j2 is const int.
* k2 is const int&.

## ex 2.36
* c is int
* d is int&
* ++c = 4;
* ++d = 5.

## ex 2.37
* c is int
* d is int&
* c = 3, d = 3.

## ex 2.38
* TODO

## ex 2.39
* error: expected ‘;’ after struct definition

## ex 2.40
* Note: std::cin >> sales_data.name >> sales_data.author >> ...; can't have space in each member.

## ex 2.41

```
[~/HappyCpp/cppp/CPPP5/2]
moocos-> make
g++ -std=c++11 -Wall -g -ggdb3 -I.. -I../1 -c ex2.41.cc -o ex2.41.o
g++ -std=c++11 -Wall -g -ggdb3 -I.. -I../1 ex2.41.o -o ex2.41.exe
rm ex2.41.o
[~/HappyCpp/cppp/CPPP5/2]
moocos-> ./ex2.41.exe < data/ceshi_ex2_41 
Total revenue is: 2791.79
```

## ex 2.42
* I defined Sales_data_ex in salse_data_ex.h and included into ex2.42.cc, remembered to use header guard in every header file.
* modified makefile accordingly
* result is:

```
[~/HappyCpp/cppp/CPPP5/2]
moocos-> make
g++ -std=c++11 -Wall -g -ggdb3 -I.. -I../1 -c ex2.42.cc -o ex2.42.o
g++ -std=c++11 -Wall -g -ggdb3 -I.. -I../1 ex2.42.o -o ex2.42.exe
rm ex2.42.o
[~/HappyCpp/cppp/CPPP5/2]
moocos-> ./ex2.42.exe < data/ceshi_ex2_41 
revenue for: 234-xxx is 199.9
revenue for: 235-xxx is 10989
revenue for: 234-zzz is -999
revenue for: 234-zaa is 1990
revenue for: 234-zbbb is 500.9
```