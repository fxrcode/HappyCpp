#include <iostream>

int main()
{
	int a = 1, b = 2, c = 3, d = 4;
	const int e = 5, f = 6;

	// int *nptr = &d;
	int i, *const cp = &a;
	int *p1, *const p2 = &b;
	const int ic = c, &r = ic;
	const int *const p3 = &d;
	const int *p;

	i = ic;
	// p1 = p3;

	p1 = &ic;
	// p3 = &ic;
	p2 = p1;
	ic = *p3;
	return 0;
}