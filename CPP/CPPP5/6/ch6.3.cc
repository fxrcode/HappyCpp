#include <iostream>
using namespace std;

int (*func(int value))[10];
int ar1[10] = {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};

int ar2[10] = {-5,-5,-5,-5,-5,-5,-5,-5,-5,-5};

int (*func(int v))[10] {
    for (size_t i = 0; i < 10; ++i) {
        ar1[i] = v+i+100;
    }
    return &ar1;
}

int main() {
    // int (*pa)[10] = func(9);
    int (*pa)[10];
    pa = func(4);
    for (size_t i = 0; i < 10; ++i) { 
        // cout << *pa[i] << endl; // this is WRONG! each i interval
        //  is 0x28 byte since pa is pointer to array of 10 int.
        cout << &ar1[i] << "--" << &(*pa)[i] << "\t";  

        // arrP points to an array, 
		// we must dereference the pointer to get the array itself
        cout << ar1[i] << "--" << (*pa)[i] << endl;  // learned from arrRet.cc.
    }
    return 0;
}


/**
(gdb) info locals
i = 4196800
pa = 0x601080 <arr>
(gdb) ptype arr
type = int [10]
(gdb) ptype pa
type = int (*)[10]
(gdb) ptype func
type = int (*(int))[10]
(gdb) ptype &arr
type = int (*)[10]

p1@p1-M32CD:~/xuexi/HappyCpp/CPP/CPPP5/6$ ./ch6.3.exe
&arr[i] --&pa[i]
0x601080--0x601080
0x601084--0x6010a8
0x601088--0x6010d0
0x60108c--0x6010f8
0x601090--0x601120
0x601094--0x601148
0x601098--0x601170
0x60109c--0x601198
0x6010a0--0x6011c0
0x6010a4--0x6011e8
**/