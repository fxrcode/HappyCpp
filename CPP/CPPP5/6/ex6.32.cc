#include <iostream>

using namespace std;

// of cause, you can return any data, the temp is init in call site.
int &get(int *array, int index) {
    return array[index];
}

int main() {
    int ia[10];
    for (int i = 0; i != 10 ; i++) {
        get(ia,i) = i;
    }
    for (const auto &elem : ia) {
        cout << elem << " ";
    }
    cout << endl;
    return 0;
}