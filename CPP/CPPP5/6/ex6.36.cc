#include <iostream>
using namespace std;

string arr[10] = {"god","god","god","god","god","god","god","god","god","god"};
string (&pa)[10] = arr;

string (&func(string t))[10] {
    // arr = {t,t,t,t,t,t,t,t,t,t};  // why it doesn't work?
    for (size_t i = 0; i < 10; ++i) {
        arr[i] = t;
    }
    return arr;
}

int main() {
    string (&pa)[10] = func("Love");
    for (size_t i = 0; i < 10; ++i) {
        cout << (pa)[i] << endl;
    }
    return 0;
}