#include <iostream>
using namespace std;

int abss(int val) {
    return val > 0 ? val : -val;
}

int main() {
    cout << "Type in a val for abs()" << endl;
    int ip;
    cin >> ip;
    cout << abss(ip) << endl;
    return 0;
}