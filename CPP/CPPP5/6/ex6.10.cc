#include <iostream>

using namespace std;

void swap2(int* a, int *b) {
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

int main() {
    int v1 = 10, v2 =100;
    cout << "Please type in 2 values\n";
    cin >> v1 >> v2;
    swap2(&v1, &v2);
    cout << v1 << ", " << v2 << endl;
    return 0;
}