#include <iostream>
using namespace std;

// https://stackoverflow.com/questions/13081837/reference-to-an-array-in-c
typedef string (&rstr10)[10];
using rArr = &string[10];

// direct declare
string (&func0(string str))[10];
// trailing return
auto func1(string str) -> string (&)[10];
// use typedef 
rstr10 func2(string str);
// use decltype
string arrS[10];
decltype(arrS)& func3(rArr& arr);

int main() {
    return 0;
}