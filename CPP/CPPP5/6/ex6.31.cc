#include <iostream>

using namespace std;

const int PI = 31415;

// of cause, you can return any data, the temp is init in call site.
int test0() {
    int i = 42;
    return i;
}

// return i; NEVER return pointer/reference to local objects.
const int &test1() {
    int i = 42;
    return PI;
}

int main() {
    int n = test();
    cout << n << endl;
    return 0;
}