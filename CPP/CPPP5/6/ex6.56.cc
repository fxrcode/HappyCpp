#include <iostream>
#include <vector>

using namespace std;

using FP = int (*)(int, int);
std::vector<FP> vFP;

int add(int a, int b) {
    return a+b;
}
int substract(int a, int b) {
    return a-b;
}
int multiply(int a, int b) {
    return a*b;
}
int divide(int a, int b) {
    return a/b;
}

int main() {
    vFP.push_back(add);
    vFP.push_back(substract);
    vFP.push_back(multiply);
    vFP.push_back(divide);

    int a = 10, b = 2;
    for (auto fp : vFP) {
        cout << (*fp)(a,b) << endl;
    }

    return 0;
}