#include <iostream>
#include <vector>
#include <iterator>

using namespace std;
using Iter = vector<int>::iterator;


void pr(int id, const int arr[]) {
    if (id == -1) {
        cout << endl;
        return;
    }
    cout << arr[id] << " ";
    pr(--id, arr);
}

void prv(Iter first, Iter last) {
    if (first == last) {
        cout << endl;
        return;
    }
        
    cout << *first << " ";
    prv(++first, last);
}

int main() {
    // int ia[10] = {1,2,3,4,5,6,7,8,9,10};
    // pr(9, ia);

    vector<int> vec {1,2,3,4,5,6,7,8,9,10};
    prv(vec.begin(), vec.end());
    return 0;
}