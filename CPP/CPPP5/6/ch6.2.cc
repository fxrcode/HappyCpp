#include <iostream>
#include <iterator>
using namespace std;

void print(const int *beg, const int *end) {
    while (beg != end) {
        cout << *beg++ << endl;
    }
}

void print(const int ia[], size_t size) {
    for (size_t i = 0; i < size; ++i) {
        cout << ia[i] << endl;
    }
}

void print(int (&arr)[8]) {
    for (auto elem : arr) {
        cout << elem << endl;
    }
}

void test() {
    int j[] = {4,5,6,7};
    cout << j << endl;
    cout << end(j) << endl;
    cout << begin(j) << endl;
    cout << end(j) - begin(j) << endl;
}

int main() {
    int hello[] = {-5, 3, 4, 5, 6, 7, 8, 100};
    // print(begin(hello), end(hello));
    // print(hello, end(hello) - begin(hello));
    print(hello);
    return 0;
}