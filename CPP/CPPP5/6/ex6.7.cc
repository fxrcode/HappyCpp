#include <iostream>
using namespace std;

int inc_calls() 
{
    static size_t cnt = 0;
    return cnt++;
}
int main() 
{
    int upper = 0;
    cout << "Please give the upper bound: " << endl;
    cin >> upper;
    for (int i = 0; i < upper; ++i) {
        cout << inc_calls() << endl;
    }
    return 0;
}