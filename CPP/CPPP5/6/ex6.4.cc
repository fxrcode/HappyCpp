#include <iostream>
using std::cout; using std::cin; using std::endl;

// int[] tony(int v, int n) {
// 	int[] arr = new int[n];
// 	for (int i = 0; i < n; ++i) {
// 		arr[i] = v;
// 	}
// 	return arr;
// }

int fact(int i) {
    int ret = 1;
    while (i > 0) {
        ret *= i--;
    }
    return ret;
}

int main() {
    cout << "Please type in a value for fact!" << endl;
    int val = 0;
    cin >> val;
    int ret = fact(val);
    cout << "The result is: " << ret << endl;
	// int[] ret = tony(5,10);
    return 0;
}