#include <iostream>
using namespace std;

int main(int argc, char **argv) {
    for (size_t i = 0; i < argc; ++i) {
        // don't use *argv[i], this is equal to argv[i][0]
        cout << argv[i] << endl;  
    }
    return 0;
}