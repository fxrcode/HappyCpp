#include <iostream>
#include <string>
#include "newScreen.h"
#include "WindowMgr.h"

using namespace std;

int main() {
    Screen myScreen(5,3,'@');
	// move the cursor to a given position, and set that character 
	myScreen.move(4,0).set('#');

	Screen nextScreen(5, 5, 'X');
	nextScreen.move(4,0).set('0').display(cout);
	cout << "\n";
	nextScreen.display(cout);
	cout << endl;

    Window_mgr wm;
    wm.addScreen(myScreen);
    wm.addScreen(nextScreen);

    wm.printAll();

    return 0;
}