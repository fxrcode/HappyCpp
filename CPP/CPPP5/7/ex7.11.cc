#include <iostream>
using namespace std;

class Sales_data {
    friend istream &read(istream &is, Sales_data&);
    friend ostream &print(ostream &os, const Sales_data&);
    public:
        Sales_data() = default;
        Sales_data(istream &is);
    private:
        std::string isbn;
        unsigned units_sold = 0;
        double revenue = 0.0;
};

Sales_data::Sales_data(istream &is) {
    read(is, *this);
}

istream &read(istream &is, Sales_data &item) {
    double price;
    is >> item.isbn >> item.units_sold >> price;
    item.revenue = price * item.units_sold;
    return is; 
}

ostream &print(ostream &os, const Sales_data &item) {
    os << item.isbn << " " << item.revenue;
    return os;
}

int main() {
    Sales_data sd(cin);
    print(cout, sd);
    return 0;
}