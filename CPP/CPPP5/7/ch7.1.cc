#include <iostream>
#include <string>
#include "Sales_data.h"

using std::cout; using std::endl; using std::cin;

class Tony {
    friend std::ostream &print(std::ostream&, const Tony &t);
    // friend std::istream &read(std::istream, std::string n);
    private:
        std::string name;
    public:
        Tony() = default;
        Tony(std::string n):name(n) {
            cout << "ctor called" << endl;
        };
        void test();
        std::string getName();
};

void Tony::test() {
    cout << "God's name: " << this->name << endl;
}

// Always double check the signature, it's reference to os!!!
std::ostream &print(std::ostream& os, const Tony &tony) {
    os << "My name is: " << tony.name << endl;
    return os;
}

int main() {
    // Tony t("tonyy");
    // print(cout,t);
    // t.test();
    Sales_data item4(std::cin);
    print(std::cout, item4) << std::endl;
    return 0;
}