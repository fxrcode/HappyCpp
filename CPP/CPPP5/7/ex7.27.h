#include <iostream>
#include <string>

using namespace std;

class Screen {
    public:
        using pos = std::string::size_type;
        Screen() = default;
        Screen(pos ht = 0, pos wd = 0): 
            height(ht), width(wd), contents(ht * wd, ' ') {}
        Screen(pos ht, pos wd, char c): 
            height(ht), width(wd), contents(ht * wd, c) {}
        char get() const { return contents[cursor]; }
        char get(pos r, pos c) const { return contents[r*width + c]; }
        
        // need to implement 3 methods
        Screen &move(pos r, pos c);
        Screen &set(char);
        Screen &set(pos r, pos c, char ch);

        // ver 1 display, converse non-const this to const
        Screen &display(std::ostream& os) {
            do_display(os);
            return *this;
        }

        // ver 2 display, IO must use reference!
        const Screen &display(std::ostream& os) const {
            do_display(os);
            return *this;
        }
    private:
        std::string contents;
        pos cursor = 0;
        pos width = 0, height = 0;

        // hide implementation, refer to const this.
        void do_display(std::ostream& os) const {
            os << "Display the screen: " << endl;
            os << contents;
        }
};

// return *this as lvalue for chaining methods call pattern.
Screen &Screen::move(pos r, pos c) {
    cursor = r * width + c;
    return *this;
}

Screen &Screen::set(char ch) {
    contents[cursor] = ch;
    return *this;
}

Screen &Screen::set(pos r, pos c, char ch) {
    contents[r * width + c] = ch;
    return *this;
}