#include <iostream>
#include <string>
#include "ex7.27.h"

using namespace std;
int main() {
    Screen myScreen(5,5,'T');
    myScreen.display(cout);
    cout << endl;
    myScreen.move(4,0).set('N').display(std::cout);
    cout << endl;
    cout << "Hello world!" << endl;
    return 0;
}