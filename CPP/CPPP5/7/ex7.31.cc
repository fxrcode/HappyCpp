#include <iostream>
using namespace std;

class Y;  // class declaration

class X {
    friend ostream &print(ostream &os, X &x);
    public:
        X() = default;
        X(int v):xi(v) {}
    private:
        Y *py = nullptr;
        int xi = -100;
};

class Y {
    friend ostream &print(ostream &os, Y &y);
    public:
        Y() = default;
        Y(int xv):xo(xv) {}
    private:
        std::string ys = "I love u";
        X xo = -99;
};


ostream &print(ostream &os, X &x) {
        os << "Print X: " << x.xi;
        if (x.py != nullptr)
            print(os, *(x.py));
        return os;
}

ostream &print(ostream &os, Y &y) {
        os << "Print Y: " << y.ys << "\t";
        print(os, y.xo);
        return os;
}

int main() {
    X x(3);
    Y y(42);
    print(cout, x);
    cout << endl;
    print(cout, y);
    cout << endl;
    return 0;
}