#include <iostream>
#include <string>
#include "Sales_data.h"

using std::cout; using std::endl; using std::cin;

int main() {
    Sales_data total;
    if (read(cin, total)) {
        Sales_data trans;
        while (read(cin, trans)) {
            total = add(total, trans);
        }
    }
    else {
        cout << "No data?!" << endl;
        return -1;
    }
    print(cout, total);
    cout << endl;
    return 0;
}