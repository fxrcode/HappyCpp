#include <iostream>
#include "ex7.32.h"

using namespace std;
int main()
{
    Screen myScreen(5, 3, 'Y');
    // myScreen.move(4, 0).set('#');

    Screen nextScreen(5, 5, 'X');
    // nextScreen.move(4, 0).set('#').display(cout);
    // cout << "\n";
    // nextScreen.display(cout);
    // cout << endl;

    Window_mgr wm;
    auto myid = wm.addScreen(&myScreen);
    auto nextid = wm.addScreen(&nextScreen);
    cout << myid << " " << nextid << endl;
    wm.clear(myid);
    wm.clear(nextid);

    myScreen.display(cout);
    cout << "\n";
    nextScreen.display(cout);
    cout << endl;

    return 0;
}