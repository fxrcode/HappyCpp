/*
=================================================================================
C++ Primer 5th Exercise Answer Source Code
ex7.21

This is the unit test
=================================================================================
*/

#include <iostream>
#include "ex7.21.h"

using namespace std;

int main() {
    cout << "hello from ex7.21.cc" << endl;
    Sales_data sd = Sales_data{"Deep Learning", 10, 70.00};

    // why is this print use the friend function in ex7.21, without :: or .?
    print(cout, sd);
    return 0;
}