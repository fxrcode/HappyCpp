/*
 * This file contains code from "C++ Primer, Fifth Edition", by Stanley B.
 * Lippman, Josee Lajoie, and Barbara E. Moo, and is covered under the
 * copyright and warranty notices given in that book:
 * 
 * "Copyright (c) 2013 by Objectwrite, Inc., Josee Lajoie, and Barbara E. Moo."
 * 
 * 
 * "The authors and publisher have taken care in the preparation of this book,
 * but make no expressed or implied warranty of any kind and assume no
 * responsibility for errors or omissions. No liability is assumed for
 * incidental or consequential damages in connection with or arising out of the
 * use of the information or programs contained herein."
 * 
 * Permission is granted for this code to be used for educational purposes in
 * association with the book, given proper citation if and when posted or
 * reproduced.Any commercial use of this code requires the explicit written
 * permission of the publisher, Addison-Wesley Professional, a division of
 * Pearson Education, Inc. Send your request for permission, stating clearly
 * what code you would like to use, and in what specific way, to the following
 * address: 
 * 
 * 	Pearson Education, Inc.
 * 	Rights and Permissions Department
 * 	One Lake Street
 * 	Upper Saddle River, NJ  07458
 * 	Fax: (201) 236-3290
*/ 

#include "Debug.h"
// only implementation for the Debug classes are definitions
// for static members named enable 
constexpr Debug HW_Subsystem::enable;
constexpr Debug IO_Subsystem::enable;

/** Got error if compile with c++14. So I turn back to c++11
g++ -std=c++14 -Wall -g -ggdb3 -I.. -I../1 -c Debug.cc -o Debug.o
In file included from Debug.cc:30:0:
Debug.h: In member function ‘bool HW_Subsystem::default_debug()’:
Debug.h:54:43: error: passing ‘const Debug’ as ‘this’ argument discards qualifiers [-fpermissive]
  bool default_debug() { return enable.any() && debug.any(); }
                                           ^
Debug.h:37:17: note:   in call to ‘constexpr bool Debug::any()’
  constexpr bool any() { return hw || io || other; }
                 ^
Debug.h: In member function ‘bool IO_Subsystem::default_debug()’:
Debug.h:65:45: error: passing ‘const Debug’ as ‘this’ argument discards qualifiers [-fpermissive]
  bool default_debug()   { return enable.any() && debug.any(); }
                                             ^
Debug.h:37:17: note:   in call to ‘constexpr bool Debug::any()’
  constexpr bool any() { return hw || io || other; }
                 ^
../GNU_makefile_template:42: recipe for target 'Debug.o' failed
make: *** [Debug.o] Error 1

**/