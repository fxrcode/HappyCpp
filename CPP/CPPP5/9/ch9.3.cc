#include <array>
using std::array;

#include <string>
using std::string;

#include <list>
using std::list;

#include <vector>
using std::vector;

#include <iostream>
using std::cout; using std::cin;
using std::endl;

class Screen {
    private:
        int index;
        string name;
    public:
        Screen() = default;
        Screen(int i, string n): index(i), name(n) {};
        int geti() const { return index; }
        int seti(int i) { index = i; return i; }
        string getn() { return name; }
};

void ch9_3() {
    vector<string> svec = {"v1", "v2", "v3", "v4", "v5"};
    list<string> slist = {"l1", "l2"};
    // slist.insert(slist.begin(), "Hello!");

    // for (const auto &v : svec) {
    //     cout << v << endl;
    // }
    // for (const auto &s : slist) {
    //     cout << s << endl;
    // }

    // string word;

    // auto iter = slist.begin();
    // while (cin >> word) {
    //     iter = slist.insert(iter, word);
    // }

    // for (const auto &ls : slist) {
    //     cout << ls << endl;
    // }

    vector<int> c = {1,2,4,5,65};
    if (!c.empty()) {
        c.front() = 42;
    }
    for (const auto &i : c)
        cout << i << endl;

    vector<Screen> vs;
    Screen sh(5, "hello");
    vs.push_back(sh);
    vs.emplace_back(Screen(42, "ufo"));

    vs[0].seti(999);
    for (auto &s : vs) {
        cout << s.geti() << " " << s.getn() << endl;
    }
    cout << sh.geti() << " " << sh.getn() << endl;

}

int main()
{
    ch9_3();
    return 0;
}