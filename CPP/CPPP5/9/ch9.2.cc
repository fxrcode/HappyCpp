#include <array>
using std::array;

#include <string>
using std::string;

#include <list>
using std::list;

#include <vector>
using std::vector;

#include <iostream>
using std::cout;
using std::endl;

void ch9_2_5() {
    array<int, 10> a1 = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    array<int, 10> a2 = {1}; // elements all have value 0
    a1 = a2;                 // replaces elements in a1
    a2 = {0};                // error: cannot assign to an array from a braced list

    list<string> names;
    vector<const char*> oldstyle;
    // names = oldstyle;    // error: no match for operator=
    names.assign(oldstyle.cbegin(), oldstyle.cend());
}

int main()
{
    ch9_2_5();
    return 0;
}