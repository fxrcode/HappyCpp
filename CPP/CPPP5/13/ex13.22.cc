#include <iostream>
using std::cout;
using std::cin;

#include <string>
using std::string;

#ifndef ex13_22_h
#define ex13_22_h

class HasPtr {
public:
    HasPtr(const std::string &s=std::string()):
        ps(new std::string(s), i(0)) {}
    HasPtr(const HasPtr&) ;
    HasPtr &operator=(const HasPtr&);
    ~HasPtr() { delete ps; }
private:
    std::string *ps;
    int i;
};

HasPtr::HasPtr(const HasPtr& orig) :
    ps(new string(*orig.ps)),
    i(orig.i) {}

HasPtr &HasPtr::operattor=(const HasPtr& orig) {
    auto new_p = new std::string(*orig.ps);
    delete ps; // ttt: why?
    ps = new_p;
    i = orig.i;
    return *this;
}

#endif