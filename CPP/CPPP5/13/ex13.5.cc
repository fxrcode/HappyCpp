#include <iostream>
using std::cout;
using std::cin;

#include <string>
using std::string;

#ifndef ex13_05_h
#define ex13_05_h

class HasPtr {
public:
    HasPtr(const std::string &s=std::string()):
        ps(new std::string(s), i(0)) {}
    HasPtr(const HasPtr&) ;
private:
    std::string *ps;
    int i;
};

HasPtr::HasPtr(const HasPtr& orig) :
    ps(new string(*orig.ps)),
    i(orig.i) {}

#endif