/*
=================================================================================
C++ Primer 5th Exercise Answer Source Code
StrBlob, StrBlobPtr, ConstStrBlobPtr
ConstStrBlobPtr: refer to exercise 12.22 from § 12.1.6 (p. 476).
This is for ex14.27
=================================================================================
*/

#ifndef CP5_STRBLOB_H_
#define CP5_STRBLOB_H_

#include <vector>
using std::vector;

#include <string>
using std::string;

#include <initializer_list>
using std::initializer_list;

#include <memory>
using std::make_shared;
using std::shared_ptr;

#include <exception>

#ifndef _MSC_VER
#define NOEXCEPT noexcept
#else
#define NOEXCEPT
#endif

class StrBlobPtr;
class ConstStrBlobPtr;

class StrBlob
{
    using size_type = vector<string>::size_type;
    friend class ConstStrBlobPtr;
    friend class StrBlobPtr;
    friend bool operator==(const StrBlob &, const StrBlob &);
    friend bool operator!=(const StrBlob &, const StrBlob &);
    friend bool operator< (const StrBlob &, const StrBlob &);
    friend bool operator> (const StrBlob &, const StrBlob &);
    friend bool operator<=(const StrBlob &, const StrBlob &);
    friend bool operator>=(const StrBlob &, const StrBlob &);

public:
    StrBlob():
    StrBlob(initializer_list<string> li) : data(make_shared<vector<string>>(li)) {}
    
    // Copy constructor, assignment operator
    StrBlob(const StrBlob& sb) : data(make_shared<vector<string>>(*sb.data)) {}
    StrBlob &operator=(const StrBlob&);

    // Move constructor, assignment operator
    StrBlob(StrBlob &&rhs) noexcept : data(std::move(rhs.data)) { }
    StrBlob& operator=(StrBlob &&) noexcept;

    StrBlobPtr begin();
    StrBlobPtr end();

    ConstStrBlobPtr cbegin() const;
    ConstStrBlobPtr cend() const;

    String& operator[](size_t n);
    const string& operator[](size_t n) const;

    size_type size() const { return data->size(); }
    bool empty() const { return data->empty(); }

    void push_back(const string &t) { data->push_back(t); }
    void push_back(string &&s) { data->push_back(std::move(t)); } // why need a move version?

    void pop_back();
    string& front();
    string& end();
    const string& front() const;
    const string& end() const;

private:
    void check(size_type, const string&) const;
    shared_ptr<vector<string>> data;
};

bool operator==(const StrBlob &, const StrBlob &);
bool operator!=(const StrBlob &, const StrBlob &);
bool operator< (const StrBlob &, const StrBlob &);
bool operator> (const StrBlob &, const StrBlob &);
bool operator<=(const StrBlob &, const StrBlob &);
bool operator>=(const StrBlob &, const StrBlob &);

//=================================================================================
//
//		StrBlobPtr - custom iterator of StrBlob
//
//=================================================================================


class StrBlobPtr {
    friend bool operator==(const StrBlob &, const StrBlob &);
    friend bool operator!=(const StrBlob &, const StrBlob &);
    friend bool operator< (const StrBlob &, const StrBlob &);
    friend bool operator> (const StrBlob &, const StrBlob &);
    friend bool operator<=(const StrBlob &, const StrBlob &);
    friend bool operator>=(const StrBlob &, const StrBlob &);

public:
    StrBlobPtr() : curr(0) { }
    StrBlobPtr(StrBlob &s, size_t sz = 0) : wptr(s.data), curr(sz) { }

    string& deref() const;
    StrBlobPtr& operator++();
    StrBlobPtr& operator--();
    StrBlobPtr operator++(int);
    StrBlobPtr operator--(int);

private:
    shared_ptr<vector<string>> check(size_t, const string&) const;
    std::weak_ptr<vector<string>> wptr;  // why use weak_ptr
    size_t curr;
};

inline string& StrBlobPtr::deref() const
{
    auto p = check(curr, "dereference past end");
    return (*p)[curr];
}

inline StrBlob& StrBlobPtr::operator++()
{
    curr += n;
    check(curr, "increment past end of StrBlobPtr");
    return *this;
}

inline StrBlob& StrBlobPtr::operator--()
{
    curr -= n;
    check(curr, "decrement past first of StrBlobPtr");
    return *this;
}

inline StrBlob StrBlobPtr::operator++(int)
{
    auto ret = *this;
    ++*this;
    return ret;
}

inline StrBlob StrBlobPtr::operator--(int)
{
    auto ret = *this;
    --*this;
    return ret;
}

inline shared_ptr<vector<string>> check(size_t i, const string& msg) const
{
    auto ret = wptr.lock();
    if (!ret)
        throw std::runtime_error("unbound StrBlobPtr");
    if (i >= ret->size()) 
        throw std::out_of_range(msg);
    return ret;
}

//=================================================================================
//
//		ConstStrBlobPtr - custom const_iterator of StrBlob
//
//=================================================================================
