#include "ex14_27_StrBlob.h"
#include <algorithm>

//==================================================================
//
//		StrBlob - operators
//
//==================================================================

bool operator==(const StrBlob &lhs, const StrBlob &rhs) {
    return *lhs.data == *rhs.data;
}
bool operator!=(const StrBlob &lhs, const StrBlob &rhs) {
    return !(lhs == rhs);  // delegate operator!= to operator==
}
bool operator< (const StrBlob &lhs, const StrBlob &rhs) {
    return std::lexicographical_compare(lhs.data->begin(), lhs.data->end(),
                                rhs.data->begin(), rhs.data->end());
}
bool operator> (const StrBlob &lhs, const StrBlob &rhs) {
    return rhs < lhs;
}
bool operator<= (const StrBlob &lhs, const StrBlob &rhs) {
    return !(lhs > rhs);
}
bool operator>=(const StrBlob &lhs, const StrBlob &rhs) {
    return !(rhs < lhs);
}

//==================================================================
//
//		StrBlobPtr - operators
//
//==================================================================


//==================================================================
//
//		ConstStrBlobPtr - operators
//
//==================================================================
