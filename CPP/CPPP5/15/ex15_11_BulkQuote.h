/*
=================================================================================
C++ Primer 5th Exercise Answer Source Code
ex15.11
=================================================================================
*/

#ifndef EX15_11_BULKQUOTE_H
#define EX15_11_BULKQUOTE_H

#include "ex15_11_Quote.h"
#include <string>

inline namespace EX15_11
{
using std::string;
using std::ostream;
using std::cout;
using std::endl;

class BulkQuote : public Quote
{
  public:
    BulkQuote() = default;
    BulkQuote(string const &book, double p, size_t qty, double disc) : 
        Quote(book, p), min_qty(qty), discount(disc) { }

    virtual double net_price(size_t cnt) const override {
        if (cnt >= min_qty) 
            return price * cnt * (1-discount);
        else 
            return price * cnt;
    }
    virtual void debug() const override
    {
        Quote::debug();
        cout << "\tminqty: " << min_qty 
            << "\tdiscount: " << discount << endl;
    }

  protected:
    size_t min_qty = 0;
    double discount = 0.0;
};
}

#endif