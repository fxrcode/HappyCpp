#include <vector>
#include <numeric>
#include <memory>

#include "ex15_28_BulkQuote.h"
#include "ex15_28_Quote.h"

using std::vector; 
using std::cout;
using std::endl;
using std::shared_ptr;
using std::make_shared;

using EX28::Bulk_quote;
using EX28::Quote;

int main() {
    vector<Quote> vecQuote;
    Bulk_quote Bulk0 = Bulk_quote("Algs", 80.00, 3, 0.5);
    Bulk_quote Bulk1 = Bulk_quote("STL tutorial", 40.00, 5, 0.2);
    
    // total price should be:
    cout << "Bulk_quote total: " << Bulk0.net_price(10) + Bulk1.net_price(10) << endl;

    vecQuote.push_back(Bulk0);
    vecQuote.push_back(Quote("Deep Learning", 70.00));
    vecQuote.push_back(Bulk1);

    double total = std::accumulate(vecQuote.cbegin(), vecQuote.cend(), 0.0, 
            [](double ret, const Quote &obj) {
                double tmp = obj.net_price(10);
                cout << "Obj: " << obj.isbn() << "'s cost: " << tmp << endl;
                return ret += tmp;
            });

    // total price in the vector
    cout << "Total in the vector: (Must be wrong!!!) " << total << endl;
    return 0;
}