/*
=================================================================================
C++ Primer 5th Exercise Answer Source Code
Quote class and print_total functiong
=================================================================================
*/

#ifndef EX15_3_QUOTE_H
#define EX15_3_QUOTE_H

inline namespace EX15_3 {
    using std::string;
    using std::ostream; using std::endl;

class Quote {
public:
    Quote() = default;
    explicit Quote(string const& b, double p) : bookNo(b), price(p) { }

    string isbn() const { return bookNo; }
    virtual double net_price(size_t n) const { return n * price; }
    virtual ~Quote() = default;
private:
    string bookNo;
protected:
    double price = 0.0;
};

double print_total(ostream &os, Quote const& item, size_t n) {
    double ret = item.net_price(n);
    os << "ISBN: " << item.isbn() << " # solde: " << n 
        << " total due:" << ret << endl;
    return ret;
}

}
#endif