/*
=================================================================================
C++ Primer 5th Exercise Answer Source Code
ex15.11 main for test
=================================================================================
*/
#include <iostream>
#include <string>
#include <memory>

#include "ex15_11_BulkQuote.h"
#include "ex15_11_Quote.h"

using std::string;
using std::ostream;
using std::cout;
using std::endl;
using std::shared_ptr;
using std::make_shared;

int main() {
    Quote q("Deep Learning", 72.00);
    BulkQuote bq("CLRS", 84.00, 10, 0.1);

    Quote *qp = &q;
    Quote *bqp = &bq;

    shared_ptr<Quote> tttp = make_shared<BulkQuote>("Operating System", 127.50, 5, 0.5);

    double np2 = qp->net_price(5); // dynamic binding
    cout << "Netprice for bulk is: " << np2 << endl;
    qp->debug();

    np2 = bqp->net_price(100);
    cout << "Netprice for bulk is: " << np2 << endl;
    bqp->debug();

    tttp->debug();
    return 0;
}