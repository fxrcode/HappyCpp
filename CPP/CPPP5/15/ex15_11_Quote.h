/*
=================================================================================
C++ Primer 5th Exercise Answer Source Code
ex15.11
=================================================================================
*/

#ifndef EX15_11_QUOTE_H
#define EX15_11_QUOTE_H

#include <iostream>
#include <string>

inline namespace EX15_11
{
using std::string;
using std::ostream;
using std::cout;
using std::endl;

class Quote
{
  public:
    Quote() = default;
    explicit Quote(string const &b, double p) : bookNo(b), price(p) {}

    string isbn() const { return bookNo; }
    virtual double net_price(size_t n) const { return n * price; }
    virtual ~Quote() = default;
    virtual void debug() const;

  private:
    string bookNo;

  protected:
    double price = 0.0;
};

void Quote::debug() const
{
    cout << "Debug Info: data members: \n"
         << "\tbookNo: " << bookNo
         << "\tprice: " << price << endl;
}

double print_total(ostream &os, Quote const &item, size_t n)
{
    double ret = item.net_price(n);
    os << "ISBN: " << item.isbn() << " # solde: " << n
       << " total due:" << ret << endl;
    return ret;
}
}
#endif