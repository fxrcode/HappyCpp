#include <vector>
#include <numeric>
#include <memory>

#include "ex15_28_BulkQuote.h"
#include "ex15_28_Quote.h"

using std::vector; 
using std::cout;
using std::endl;
using std::shared_ptr;
using std::make_shared;

using EX28::Bulk_quote;
using EX28::Quote;

int main() {
    vector<shared_ptr<Quote>> vecQuote;
    shared_ptr<Bulk_quote> spBulk0 = make_shared<Bulk_quote>("Algs", 80.00, 3, 0.5);
    shared_ptr<Bulk_quote> spBulk1 = make_shared<Bulk_quote>("STL tutorial", 40.00, 5, 0.2);
    shared_ptr<Quote> spQuote = make_shared<Quote>("Deep Learning", 70.00);
    
    // total price should be:
    cout << "Bulk_quote total: " 
        << spBulk0->net_price(10) + spBulk1->net_price(10) + spQuote->net_price(10)<< endl;

    vecQuote.push_back(spBulk0);
    vecQuote.push_back(spQuote);
    vecQuote.push_back(spBulk1);

    double total = std::accumulate(vecQuote.cbegin(), vecQuote.cend(), 0.0, 
            [](double ret, shared_ptr<Quote> sp) {
                return ret += sp->net_price(10);
            });

    // total price in the vector
    cout << "Total in the vector: " << total << endl;
    return 0;
}