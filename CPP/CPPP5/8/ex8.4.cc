#include <iostream>
using std::cerr;
using std::cout;
using std::endl;

#include <fstream>
using std::ifstream;

#include <string>
using std::string;

#include <stdexcept>
using std::runtime_error;

#include <vector>
using std::vector;

class ex8_4 {
    private:
        vector<string> bibles;
    public:
        ex8_4() = default;
        int read(const string &);
        void printb();
};

void ex8_4::printb() {
    for (string b : bibles) {
        cout << b << endl;
    }
}

int ex8_4::read(const string &filepath) {
    ifstream ifs(filepath);
    int lines = 0;
    if (ifs) {
        string buf;
        while (getline(ifs, buf)) {
            bibles.push_back(buf);
            lines++;
        }
    }
    return lines;
}

// use this in terminal './ex8.4.exe bible.txt'
int main(int argc, char *argv[])
{
    if (argc != 2) {
        cout << "Please give file path" << endl;
        return -1;
    }
    ex8_4 ex;
    cout << argv[1] << endl;
    int lines = ex.read(argv[1]);
    cout << lines << endl;
    ex.printb();
    return 0;
}