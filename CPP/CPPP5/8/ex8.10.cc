#include <iostream>
using std::cin;
using std::cout;
using std::cerr;
using std::istream;
using std::ostream;
using std::endl;

#include <fstream>
using std::ifstream;

#include <sstream>
using std::ostringstream;
using std::istringstream;

#include <vector>
using std::vector;

#include <string>
using std::string;

// fxr: https://stackoverflow.com/questions/16620584/why-can-i-not-pass-an-ifstream-as-i-construct-it
vector<string> getDataRVal(ifstream &&ifs)
{
    string line;
    vector<string> res;
    if (!ifs) {
        cout << "Fuck you" << endl;
        return res;
    }
    while (getline(ifs, line))
    {
        res.push_back(line);
    }
    return res;
}

vector<string> getDataLVal(ifstream &ifs)
{
    string line;
    vector<string> res;
    if (!ifs) {
        cout << "Fuck you" << endl;
        return res;
    }
    while (getline(ifs, line))
    {
        res.push_back(line);
    }
    return res;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        cout << "Cao, I need a file path as args" << endl;
        return -1;
    }
    auto p = argv+1;
    // option 1: input variable as parameter.
    // ifstream input(*p);
    // vector<string> lines = getDataLVal(input);

    // option 2: use ifstream&& in getDataRVal to deal with rval.
    vector<string> lines = getDataRVal(ifstream(*p));

    // failed... Cuz ifstream constructor
    // vector<string> lines = getDataLVal(ifstream(*p));
    for (auto &line : lines)
    {
        istringstream iss(line);
        string word;
        while (iss >> word) {
            cout << word << " ";
        }
    }
    return 0;
}

/*
ex8.10.cc:69:52: error: invalid initialization of non-const reference of type ‘std::ifstream& {aka std::basic_ifstream<char>&}’ from an rvalue of type ‘std::ifstream {aka std::basic_ifstream<char>}’
     vector<string> lines = getDataLVal(ifstream(*p));
                                                    ^
*/