#include <iostream>
#include <vector>
#include <stack>
#include <climits>

using namespace std;

bool find132(vector<int> &nums) {
    int third = INT_MIN;
    stack<int> s;
    for (int i = nums.size() - 1; i >= 0; --i) {
        if (nums[i] < third)  
            return true;
        else {
            while (!s.empty() && nums[i] > s.top()) {
                third = s.top();
                s.pop();
            }
        }
        s.push(nums[i]);
    }
    return false;
}

int main() {
    vector<int> v = {-10, 3, -4, 2};
    bool ans = find132(v);
    cout << ans << endl;

    return 0;
}