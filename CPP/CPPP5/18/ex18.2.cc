/**
 * Exercise 18.2: Explain what happens if an exception occurs at the indicated point:
 * 
 * valgrind -v --leak-check=yes ./ex18.2.exe
 */
#include <vector>
using std::vector;

#include <iostream>
using std::cout; using std::endl; 

#include <fstream>
using std::ifstream;

#include <exception>
using std::exception;

template <typename T>
void ignore(T &&) { }

void exercise(int *b, int *e) {
  cout << "Hi: exercise called" << endl;
  vector<int> v(b,e);

  cout << "vector v size() is: " << v.size() << endl;
	int *p = new int[v.size()];
	ifstream in("ints");
  // exceoption occurs here
}

int main() {
  // int b_wrong[] = {1,2,3,4,5};
  // int e[] = {6,7,8};
  // ignore(b_wrong);
  int *b = new int[5]{1,2,3,4,5};
  int *e = new int[2]{6,7};
  exercise(b, e);

  delete[] b;
  delete[] e;

  return 0;
}


/*********************************
==15450== LEAK SUMMARY:
==15450==    definitely lost: 96 bytes in 1 blocks
==15450==    indirectly lost: 0 bytes in 0 blocks
==15450==      possibly lost: 0 bytes in 0 blocks
==15450==    still reachable: 72,704 bytes in 1 blocks
==15450==         suppressed: 0 bytes in 0 blocks
==15450== Reachable blocks (those to which a pointer was found) are not shown.
==15450== To see them, rerun with: --leak-check=full --show-leak-kinds=all
==15450== 
==15450== ERROR SUMMARY: 10 errors from 3 contexts (suppressed: 0 from 0)
 */