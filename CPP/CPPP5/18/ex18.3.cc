/**
 * Exercise 18.2: Explain what happens if an exception occurs at the indicated point:
 * 
 * valgrind -v --leak-check=yes ./ex18.2.exe
 */
#include <memory>
using std::shared_ptr;

#include <vector>
using std::vector;

#include <iostream>
using std::cout; using std::endl; 

#include <fstream>
using std::ifstream;

#include <exception>
using std::exception;

template <typename T>
void ignore(T &&) { }

void exercise(int *b, int *e) {
  cout << "Hi: exercise called" << endl;
  vector<int> v(b,e);

  cout << "vector v size() is: " << v.size() << endl;
  // int *p = new int[v.size()];
  shared_ptr<int> p(
    new int[v.size()],
    [](int *p) { delete[] p; }
  );
	ifstream in("ints");
  // exceoption occurs here
}

int main() {
  // int b_wrong[] = {1,2,3,4,5};
  // int e[] = {6,7,8};
  // ignore(b_wrong);
  int *b = new int[5]{1,2,3,4,5};
  int *e = new int[2]{6,7};
  exercise(b, e);

  delete[] b;
  delete[] e;

  return 0;
}

/**************************************************** 
==14298== LEAK SUMMARY:
==14298==    definitely lost: 0 bytes in 0 blocks
==14298==    indirectly lost: 0 bytes in 0 blocks
==14298==      possibly lost: 0 bytes in 0 blocks
==14298==    still reachable: 72,704 bytes in 1 blocks
==14298==         suppressed: 0 bytes in 0 blocks
==14298== Reachable blocks (those to which a pointer was found) are not shown.
==14298== To see them, rerun with: --leak-check=full --show-leak-kinds=all
==14298== 
==14298== ERROR SUMMARY: 9 errors from 2 contexts (suppressed: 0 from 0)
 */