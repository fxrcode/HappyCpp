/**
* reload operator as friend function
*/

#include <iostream>

using namespace std;

class Complex {
    // need this + to do c = 5 + c;
    friend Complex operator+ (double r, const Complex &c);
    friend ostream& operator<< ( ostream &os, Complex &c);
    private:
        double real, imag;
    public:
        Complex( double r, double i): real(r),imag(i){};
        Complex operator+(double r);
};

Complex Complex::operator+(double r) {
    return Complex(real + r, imag);
}

Complex operator+ (double r, const Complex &c) {
    return Complex(r+c.real, c.imag);
}

ostream& operator<< ( ostream &os, Complex &c) {
    os << "Real: "<< c.real << ", Imag: "<< c.imag;
}

int main() {
    Complex c(5.0, -4.0);
    c = c+5;
    cout << c << endl;
    c = -9 + c;
    cout << c << endl;
    return 0;
}