#include <iostream>
#include <string>

using namespace std;

class Complex {
    double real, imag;
    public:
        Complex(double r, double i):real(r),imag(i){};
        // ~Complex();
    friend ostream& operator<<( ostream & os, const Complex & c);
    friend istream& operator>>( istream & is, Complex & c);
};

ostream & operator<< (ostream &os, const Complex &c) {
    os << c.real << "+" << c.imag << "i" << endl;
    return os;
}

istream & operator>> (istream &is, Complex &c) {
    string s;
    is >> s;
    int pos = s.find("+", 0);
    string sTmp = s.substr(0, pos);
    c.real = atof(sTmp.c_str());
    sTmp = s.substr(pos+1, s.length()-pos-2);
    c.imag = atof(sTmp.c_str());
    return is;
}

int main()
{
    Complex c(0,1);
    int n;
    cin >> c >> n;
    cout << c <<n <<endl;
    return 0;
}