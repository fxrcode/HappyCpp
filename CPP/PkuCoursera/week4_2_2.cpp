#include <iostream>
#include <cstring>

using namespace std;

class CArray {
    int size;
    int *ptr;
    public:
        CArray(int s = 0);
        CArray(CArray &a);
        ~CArray();
        void push_back(int v);
        CArray & operator=( const CArray &a);
        int length() { return size; } 
        int printCArray();
        int & operator[](int i);
};

CArray::CArray(int s):size(s)
{
    if (s == 0) 
        ptr = NULL;
    else 
        ptr = new int[s];
}

CArray::CArray(CArray &a)
{
    if (!a.ptr) {
        ptr = NULL;
        size = 0;
        return;
    }
    ptr = new int[a.size];
    memcpy( ptr, a.ptr, sizeof(int) * a.size);
    size = a.size;
}

CArray::~CArray()
{
    if (ptr) delete [] ptr;
}

CArray & CArray::operator=( const CArray &a)
{
    // trick for operator=. eg: a = a.
    if (ptr == a.ptr)  
        return *this;
    if (a.ptr == NULL) {
        if (ptr) delete [] ptr;
        ptr = NULL;
        size = 0;
        return *this;
    }
    if (size < a.size) {
        if (ptr) delete [] ptr;
        ptr = new int[a.size];
    }
    memcpy (ptr, a.ptr, sizeof(int) * a.size);
    size = a.size;
    return *this;
}

void CArray::push_back(int v) {
    if (ptr) {
        int *tmpPtr = new int[size+1];
        memcpy (tmpPtr, ptr, sizeof(int)*size);
        delete [] ptr;
        ptr = tmpPtr;
    }
    else 
        ptr = new int[1];
    ptr[size++] = v;
}

int CArray::printCArray() {
    cout << "This CArray: " << endl;
    for (int i = 0; i < size; ++i) {
        cout << i << "'s=" << ptr[i] << " " << endl;
    }
    return size;
}

// reference is for a[i]=4. so that operator can be lvalue
int & CArray::operator[](int i) {
    return ptr[i];
}

int main() {
    CArray a;
    for (int i = 0; i < 5; ++i) {
        a.push_back(i);
    }
    a.printCArray();
    CArray a1,a2;
    a1 = a; a1.push_back(99);
    a1.printCArray();
    a2 = a;
    a2[1] = -9;
    a2.printCArray();
    return 0;
}