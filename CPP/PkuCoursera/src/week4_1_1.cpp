#include <iostream>

using namespace std;

class Complex
{
    //https://stackoverflow.com/questions/1549930/c-equivalent-of-java-tostring
    friend ostream &operator<<(ostream &os, const Complex &cx);

  private:
    double real;
    double imag;

  public:
    Complex(double r = 0.0, double i = 0.0) : real(r), imag(i) {}
    Complex operator+(const Complex &);
    Complex operator-(const Complex &);
};

Complex Complex::operator+(const Complex &cx)
{
    return Complex(real + cx.real, imag + cx.imag);
}

Complex Complex::operator-(const Complex &cx)
{
    return Complex(real - cx.real, imag - cx.imag);
}

ostream &operator<<(ostream &os, const Complex &cx)
{
    double r = cx.real;
    double i = cx.imag;
    os << "real=" << r << ",imag=" << i;
    return os;
}

int main()
{
    Complex x, y(4.3, 8.2), z(3.3, 1.1);
    x = y + z;
    cout << x << endl;
    x = y - z;
    cout << x << endl;
    return 0;
}