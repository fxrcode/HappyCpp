#include <iostream>
#include <string>

using namespace std;

class Bug {
    private:
        int nLegs; int nColor;
    public:
        int nType;
        Bug (int legs, int color);
        void printBug() {
            cout << "I'm a bug: legs="<<nLegs << ",nColor="<<nColor;
        };
};

class Skill {
    public:
        Skill(int n) {
            cout << "Skill constructed=" << n << endl;
        }
};

class FlyBug: public Bug {
    int nWings;
    Skill s1, s2;
    public:
        FlyBug(int l, int c, int w);
        int getWings() { return nWings; }
        void printBug();
};

Bug::Bug(int l, int c) {
    nLegs = l;
    nColor = c;
}

FlyBug::FlyBug(int l, int c, int w)
    :Bug(l, c), s1(5), s2(c) { 
    nWings = w;
}

void FlyBug::printBug() {
    Bug::printBug();
    cout << ",nWings="<<this->getWings() << endl;
}

int main() {
    FlyBug fb(2,3,4);
    fb.printBug();
    fb.nType = 1;
    // fb.nLegs = 2;
    return 0;
}