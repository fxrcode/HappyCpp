#include <iostream>
#include <string>

using namespace std;

class base {

};

class derived:public base {

};

int main() {
	cout << "Hello world!" << endl;
	base b;
	derived d;

	// all these 3 only works if derived is public!
	b = d;
	base & br = d;
	base * bp = &d;

	return 0;
}
