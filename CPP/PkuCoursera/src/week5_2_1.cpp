#include <iostream>
#include <string>
using namespace std;

class base {
    int j;
    public:
        int i;
        void func();
};

class derived: public base {
    public:
        int i;
        void access();
        void func();
};

void derived::access() {
    // j = 5;
    i = 5;
}

int main() {
    derived d;
    d.access();

    cout << "Hello" << endl;
    return 0;
}