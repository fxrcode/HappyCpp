#include <iostream>
using namespace std;

class Point 
{
private:
	int x,y;
public:
	Point(int x1 = -1, int y1 = -1) {
		cout << "Called ctor" << endl;
		x = x1;
		y = y1;
	}

	Point(const Point &t) {
		cout << "Called copy ctor" << endl;
		// x = t.x;
		// y = t.y;
	}

	int getX() { return x; }
	int getY() { return y; }
};

Point Func1() {
	Point dian(99, 999);
	return dian;
}

Point Func0(Point a1) {
	Point b(a1);
	return b;
}

int main()
{
	/*
	Point p1(10, 15);
	Point p2(p1);
	// Point *parray[4] = { NULL, NULL, new Point(5,9), new Point(-4,-10) };

	// Let us access values assigned by constructors
	cout << "p1.x = " << p1.getX() << ", p1.y = " << p1.getY() << endl;
	cout << "p2.x = " << p2.getX() << ", p2.y = " << p2.getY() << endl;
	*/
	Point a2(9,14);
	// Func0(a2);
	cout << Func0(a2).getX() << endl;

	return 0;
}