#include <iostream>
#include <string>

class Father {
    private: int nPrivate;
    public: int nPublic;
    protected: int nProtected;
};

class Son: public Father {
    void AccessFather() {
        nPublic = 1;
        // nPrivate = 2;
        nProtected = 1;
        Son f;
        // f.nProtected = 1;
    }
};

int main() {
    Father f;
    Son s;
    f.nPublic = 1;
    s.nPublic = 1;
    f.nProtected  = 1;
    f.nPrivate = 1;
    s.nProtected = 11;
    s.nPrivate = 1;
    return 0;
}

