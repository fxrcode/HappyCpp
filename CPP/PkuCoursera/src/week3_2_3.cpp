#include <iostream>
using namespace std;

class Ctest {
    public:
        Ctest() {
            cout << "ctor called" << endl;
        }
        ~Ctest() {
            cout << "dtor called" << endl;
        }
};

class Demo {
    int id;
public:
    Demo(int i) {
        id = i;
        cout << "id=" << id << " Constructed" << endl;
    }        
    ~Demo() {
        cout << "id="<<id << " Destructed"<< endl;
    }
};

Demo globVar(1);

void func0() {
    static Demo sVar(2);
    Demo lVar(3);
    cout << "Func" << endl;
}

int main()
{
    Demo d4(4);
    d4 = 6;
    cout << "main" << endl;
    { Demo localVar(21); }
    func0();
    cout << "main ends" << endl;

    /*
    Ctest *pTest = new Ctest[5];
    // Ctest arr[2];
    cout << "End main" << endl;
    delete [] pTest;
    */
    return 0;
}