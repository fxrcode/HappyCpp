/* friend function and friend class */
#include <iostream>
using namespace std;

class CCar;
class CDriver
{
  public:
    void ModifyCar(CCar *pCar);
};
class CCar
{
  private:
    int price;
    friend int MostExpensiveCar(CCar cars[], int total);
    friend void CDriver::ModifyCar(CCar *pCar);

  public:
    CCar(int p = -1)
    {
        price = p;
    }
};

void CDriver::ModifyCar(CCar *pCar)
{
    pCar->price += 1000; // modify CCar's private member
}
int MostExpensiveCar(CCar cars[], int total)
{
    int tmpMax = -1;
    for (int i = 0; i < total; ++i)
    {
        if (cars[i].price > tmpMax)
            tmpMax = cars[i].price;
    }
    return tmpMax;
}

int main()
{
    CCar cars[3];
    cars[0] = CCar(100);
    cars[2] = CCar(10000);
    cars[1] = CCar(1000);

    int price = MostExpensiveCar(cars, 3);
    cout << "The highest price=" << price << endl;
    return 0;
}