#include <iostream>
using namespace std;

class A {
    static int sv;
    int i;
public:
    A(int val) {
        i = val;
    }
    ~A() {
        cout << "Dtor" << endl;
    }
};

int main()
{
    int sizeA = sizeof(A);
    cout << "sizeA=" << sizeA << endl;
    return 0;
}