#include <iostream>
#include <string>
using namespace std;

class CDemo {
    private:
        int n;
    public:
        CDemo(int i = 0):n(i){}
        CDemo(const CDemo &);
        CDemo operator++();     // for pre++
        CDemo operator++(int);  // for post++
        operator int() { return n; }
        friend CDemo operator--(CDemo &);
        friend CDemo operator--(CDemo &, int);
};

CDemo::CDemo(const CDemo &other) {
    n = other.n;
}

CDemo CDemo::operator++() { // pre++
    n++;
    return *this;
}

// warning: reference to local variable ‘tmp’ returned
// https://stackoverflow.com/questions/19371137/c-reference-to-local-variable-returned
// CDemo & CDemo::operator++(int v) {
//     CDemo tmp(*this);
//     n += v;
//     return tmp;
// }

CDemo CDemo::operator++(int v) {
    CDemo tmp(*this);
    n ++;
    return tmp;
}

CDemo operator--(CDemo &c) { // pre--
    c.n--;
    return c;
}

CDemo operator--(CDemo &c, int v) {// post --
    CDemo tmp(c);
    c.n --;
    return tmp;
}

int main() {
    CDemo d(5);
    cout << (d++) << ",",
    cout << d << ",";
    cout << (++d) << ",";
    cout << d << endl;
    cout << (d--) << ",";
    cout << d << ",";
    cout << (--d) << ",";
    cout << d << endl;
    return 0;
}