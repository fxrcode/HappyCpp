// overload operator =
#include <iostream>
#include <string.h>

using namespace std;

class String
{
    friend ostream &operator<<(ostream &os, String &s);

  private:
    char *str;

  public:
    String() : str(NULL) {}
    const char *c_str() { return str; }
    char *operator=(const char *s);
    ~String();
};

ostream &operator<<(ostream &os, String &s)
{
    os << "The string=" << s.c_str();
    return os;
}

char *String::operator=(const char *s)
{
    if (str)
        delete[] str;
    if (s)
    {
        str = new char[strlen(s) + 1];
        strcpy(str, s);
    }
    else
    {
        str = NULL;
    }
    return str;
}

String::~String()
{
    if (str)
        delete[] str;
}

int main()
{
    String s1, s2;
    s1 = "this";
    s2 = "that";
    s1 = s2;
    cout << s1 << endl;
    return 0;
}