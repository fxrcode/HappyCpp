# pku CPP study from Coursera
* [link](https://www.coursera.org/learn/cpp-chengxu-sheji)

# Week 4: operator reload
* used AddressSanitizer to detect week4_1_2's double free.
```
p1@p1-M32CD:~/xuexi/HappyCpp/cppp/pkuCoursera/build$ ./runme
The string=that
=================================================================
==8186==ERROR: AddressSanitizer: attempting double-free on 0x60200000efd0 in thread T0:
    #0 0x7f77c8459caa in operator delete[](void*) (/usr/lib/x86_64-linux-gnu/libasan.so.2+0x99caa
)
    #1 0x401301 in String::~String() /home/p1/xuexi/HappyCpp/cppp/pkuCoursera/week4_1_2.cpp:37
    #2 0x40142c in main /home/p1/xuexi/HappyCpp/cppp/pkuCoursera/week4_1_2.cpp:42
    #3 0x7f77c7a7f82f in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x2082f)
    #4 0x401088 in _start (/home/p1/xuexi/HappyCpp/cppp/pkuCoursera/build/runme+0x401088)

0x60200000efd0 is located 0 bytes inside of 5-byte region [0x60200000efd0,0x60200000efd5)
freed by thread T0 here:
    #0 0x7f77c8459caa in operator delete[](void*) (/usr/lib/x86_64-linux-gnu/libasan.so.2+0x99caa
)
    #1 0x401301 in String::~String() /home/p1/xuexi/HappyCpp/cppp/pkuCoursera/week4_1_2.cpp:37
    #2 0x40141f in main /home/p1/xuexi/HappyCpp/cppp/pkuCoursera/week4_1_2.cpp:42
    #3 0x7f77c7a7f82f in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x2082f)

previously allocated by thread T0 here:
    #0 0x7f77c84596b2 in operator new[](unsigned long) (/usr/lib/x86_64-linux-gnu/libasan.so.2+0x
996b2)
    #1 0x401214 in String::operator=(char const*) /home/p1/xuexi/HappyCpp/cppp/pkuCoursera/week4_
1_2.cpp:27
    #2 0x4013e3 in main /home/p1/xuexi/HappyCpp/cppp/pkuCoursera/week4_1_2.cpp:44
    #3 0x7f77c7a7f82f in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x2082f)

SUMMARY: AddressSanitizer: double-free ??:0 operator delete[](void*)
==8186==ABORTING
```

# using CMake
## rm CMakeCache.txt
* so as to rerun `cmake ..` and check find_package output

## Sanitizers
* tested Address/Memory/ThreadSanitizers with CMake.
* eg: race.cpp 
```
    #2 B::~B() /home/p1/xuexi/HappyCpp/cppp/pkuCoursera/race.cpp:36 (runme+0x0000004066c9)
    #3 main::{lambda()#2}::operator()() const <null> (runme+0x000000402fe8)
    #4 _M_invoke<> /usr/include/c++/5/functional:1531 (runme+0x000000405e35)
    #5 operator() /usr/include/c++/5/functional:1520 (runme+0x000000405c3e)
    #6 _M_run /usr/include/c++/5/thread:115 (runme+0x000000405abe)
    #7 <null> <null> (libstdc++.so.6+0x0000000b8c7f)

  Previous read of size 8 at 0x7d1c0000df90 by thread T1:
    #0 main::{lambda()#1}::operator()() const <null> (runme+0x000000402f00)
    #1 _M_invoke<> /usr/include/c++/5/functional:1531 (runme+0x000000405ee0)
    #2 operator() /usr/include/c++/5/functional:1520 (runme+0x000000405c9a)
    #3 _M_run /usr/include/c++/5/thread:115 (runme+0x000000405b08)
    #4 <null> <null> (libstdc++.so.6+0x0000000b8c7f)

  Location is heap block of size 104 at 0x7d1c0000df90 allocated by main thread:
    #0 operator new(unsigned long) <null> (libtsan.so.0+0x000000025a33)
    #1 main /home/p1/xuexi/HappyCpp/cppp/pkuCoursera/race.cpp:41 (runme+0x000000403023)

  Thread T2 (tid=9724, running) created by main thread at:
    #0 pthread_create <null> (libtsan.so.0+0x000000027577)
    #1 std::thread::_M_start_thread(std::shared_ptr<std::thread::_Impl_base>, void (*)()) <null>
(libstdc++.so.6+0x0000000b8dc2)
    #2 main /home/p1/xuexi/HappyCpp/cppp/pkuCoursera/race.cpp:43 (runme+0x000000403080)

  Thread T1 (tid=9723, running) created by main thread at:
    #0 pthread_create <null> (libtsan.so.0+0x000000027577)
    #1 std::thread::_M_start_thread(std::shared_ptr<std::thread::_Impl_base>, void (*)()) <null>
(libstdc++.so.6+0x0000000b8dc2)
    #2 main /home/p1/xuexi/HappyCpp/cppp/pkuCoursera/race.cpp:42 (runme+0x000000403059)

SUMMARY: ThreadSanitizer: data race on vptr (ctor/dtor vs virtual call) /home/p1/xuexi/HappyCpp/c
ppp/pkuCoursera/race.cpp:21 A::~A()
==================
ThreadSanitizer: reported 1 warnings
```