# Build LISP in C

## Chap 4
### C preprocessor
* `#include`, and `-ledit` in compiling, link?
### makefile
* due to the mpc.c in line 23, I got error. After I comment it out, it works. Also I added the [one line you should add to every makefile](http://blog.jgc.org/2015/04/the-one-line-you-should-add-to-every.html)

```text
osboxes@osboxes:~/Tony/HappyCpp/LISP/chap4$ make
cc -std=c99 -Wall -g -ggdb3    prompt.c   -o prompt
/tmp/ccmQ4hpu.o: In function `main':
/home/osboxes/Tony/HappyCpp/LISP/chap4/prompt.c:37: undefined reference to `readline'
/home/osboxes/Tony/HappyCpp/LISP/chap4/prompt.c:38: undefined reference to `add_history'
collect2: error: ld returned 1 exit status
make: *** [prompt] Error 1
osboxes@osboxes:~/Tony/HappyCpp/LISP/chap4$ make
cc -std=c99 -Wall -g -ggdb3 prompt.c  -ledit -lm  -o prompt
osboxes@osboxes:~/Tony/HappyCpp/LISP/chap4$ make print-FILES
FILES=prompt
osboxes@osboxes:~/Tony/HappyCpp/LISP/chap4$ make print-PLATFORM
PLATFORM=Linux

```


## Chap 5
### mpc
* two step:
	1. create and name several rules with `mpc_new()`
	2. define them use `mpca_lang()`

## Chap 6
### use mpc.h
* link to mpc.h: put mpc.c into compile command
* link to math lib: put -lm into compile command
* mpc is so cool, combined with regex

##