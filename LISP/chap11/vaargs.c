#include <stdio.h>

#define FOO(...)		printf(__VA_ARGS__)
#define BAR(fmt, ...)   printf(fmt "\n", ##__VA_ARGS__)

// http://stackoverflow.com/questions/5588855/standard-alternative-to-gccs-va-args-trick
int main(int argc, char** argv) {
	FOO("this works fine\n");
	BAR("here is a log msg");
	BAR("here is a log msg with a param: %d", 42);
}
