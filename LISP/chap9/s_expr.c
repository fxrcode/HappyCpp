#include "mpc.h"

#ifdef _WIN32

static char[2048] buffer;

char* readline(char* prompt)
{
	fputs(prompt, stdout);
	fgets(buffer, 2048, stdin);

	char* cpy = malloc(strlen(buffer)+1);
	strcpy(cpy, buffer);
	cpy[strlen(cpy)-1] = '\0';
	return cpy;
}

void add_history(char* unused) {}

#else
#include <editline/readline.h>
#include <editline/history.h>
#endif

enum { LVAL_ERR, LVAL_NUM, LVAL_SYM, LVAL_SEXPR };

// http://stackoverflow.com/questions/588623/self-referential-struct-definition
typedef struct __lval {  // notice the struct name is needed for recursive type
	int type;
	long num;
	/* Error and Symbol types have some string data */
	char* err;
	char* sym;
	/* Count and pointer to a list of "lval*" */
	int count;
	struct __lval** cell;
} lval;

lval* lval_add(lval* v, lval* x);

/*    ctor which returns pointer to struct, good API   */
lval* lval_num(long x)
{
	lval* v = malloc(sizeof(lval));
	v->type = LVAL_NUM;
	v->num = x;
	return v;
}

/*   ctor a pointer to a new error lval*/
lval* lval_err(char* m)
{
	lval* v = malloc(sizeof(lval));
	v->type = LVAL_ERR;
	v->err = malloc(strlen(m)+1);  // NOTICE~
	strcpy(v->err, m);
	return v;
}

/*   ctor a pointer to a new symbol lval   */
lval* lval_sym(char* s) 
{
	lval* v = malloc(sizeof(lval));
	v->type = LVAL_SYM;
	// v->sym = s;
	v->sym = malloc(strlen(s) + 1);
	strcpy(v->sym, s);
	return v;
}

/*   ctor a pointer to a new s-expr lval    */
// lval* lval_sexpr(char* s)
lval* lval_sexpr(void) 
{
	lval* v = malloc(sizeof(lval));
	v->type = LVAL_SEXPR;
	v->count = 0;
	v->cell = NULL;
	return v;
}

void lval_del(lval* v)
{
	switch(v->type) {
		case LVAL_NUM: break;

		case LVAL_ERR: free(v->err);  break;
		case LVAL_SYM: free(v->sym);  break;

		case LVAL_SEXPR: {
			for (int i = 0; i < v->count; ++i) {
				lval_del(v->cell[i]);  // recursively free each child
			}
			free(v->cell);  // after children is freed, free the memory allocated to contain the pointers
			break;
		}
	}

	free(v);
}

lval* lval_read_num(mpc_ast_t* t)
{
	errno = 0;   // ???
	long x = strtol(t->contents, NULL, 10);
	return errno != ERANGE ?
		lval_num(x) : lval_err("Invalid Number");
}

lval* lval_read(mpc_ast_t* t) 
{
	if (strstr(t->tag, "number")) {
		return lval_read_num(t);
	}
	if (strstr(t->tag, "symbol")) {
		return lval_sym(t->contents);
	}

	lval* x = NULL;
	if (strcmp(t->tag, ">") == 0)  {
		x = lval_sexpr();
	}
	if (strstr(t->tag, "sexpr")) {
		x = lval_sexpr();
	}

	/* Fill this list with any valid expr contained within  */
	for (int i = 0; i < t->children_num; ++i) {
		if (strcmp(t->children[i]->contents, "(") == 0)  { continue; }
		if (strcmp(t->children[i]->contents, ")") == 0)  { continue; }
		if (strcmp(t->children[i]->contents, "{") == 0)  { continue; }
		if (strcmp(t->children[i]->contents, "}") == 0)  { continue; }
		if (strcmp(t->children[i]->tag, "regex") == 0)   { continue; }

		x = lval_add(x, lval_read(t->children[i]));
	}

	return x;
}

lval* lval_add(lval* v, lval* x)
{
	v->count++;
	v->cell = realloc(v->cell, sizeof(lval*) * v->count);  // reallocate memory block
	v->cell[v->count-1] = x;
	return v;
}

/*****************************************************
 *				general helper functions
 ****************************************************/
lval* lval_pop(lval* v, int i) 
{
	/*   Find the ith node    */
	lval* x = v->cell[i];

	/*   Shift memory after the item at 'i' over the top   */
	memmove(&v->cell[i], &v->cell[i+1], sizeof(lval*) * (v->count - i - 1));

	/*	Decrease the count of items in the list   */
	v->count--;

	/*	Reallocate the memory used     */
	v->cell = realloc(v->cell, sizeof(lval*) * v->count);
	return x;
}

lval* lval_take(lval* v, int i)
{
	lval* x = lval_pop(v, i);
	lval_del(v);
	return x;
}

/*  Printing Expr  */
void lval_print(lval* x) ;

void lval_expr_print(lval* v, char open, char close) 
{
	putchar(open);
	for (int i = 0; i < v->count; ++i) {
		lval_print(v->cell[i]);
		if (i != (v->count-1)) {
			putchar(' ');
		}
	}
	putchar(close);
}

void lval_print(lval* v) 
{
	switch(v->type) {
		case LVAL_NUM:  	printf("%li", v->num);  break;
		case LVAL_ERR:  	printf("Error: %s", v->err);  break;
		case LVAL_SYM:  	printf("%s", v->sym);  break;
		case LVAL_SEXPR:	lval_expr_print(v, '(', ')');  break;
	}
}

void lval_println(lval* v) {
	lval_print(v);
	putchar('\n');
}

lval* builtin_op(lval* a, char* op) 
{
	for (int i = 0; i < a->count; ++i) {
		if (a->cell[i]->type != LVAL_NUM) {
			lval_del(a);
			return lval_err("Cannot operate on non-number!");
		}
	}

	lval* x = lval_pop(a, 0);

	if ((strcmp(op, "-") == 0) && a->count == 0) {
		x->num = -x->num;
	}

	while (a->count > 0) {
		lval* y = lval_pop(a, 0);

		if (strcmp(op, "+") == 0) { x->num += y->num; }
		if (strcmp(op, "-") == 0) { x->num -= y->num; }
		if (strcmp(op, "*") == 0) { x->num *= y->num; }
		if (strcmp(op, "/") == 0) {
			if (y->num ==0) {
				lval_del(x); lval_del(y);  // why need to free?
				x = lval_err("Division by zero.");
				break;
			}
			x->num /= y->num;
		}

		lval_del(y);
	}

	lval_del(a);
	return x;
}

/*****************************************************
 *				eval functions
 ****************************************************/
/*           nice solution             */
lval* lval_eval(lval* v) ;

lval* lval_eval_sexpr(lval* v)
{
	/*  Evaluate Children   */
	for (int i = 0; i < v->count; ++i) {
		v->cell[i] = lval_eval(v->cell[i]);
	}

	/*  Error handling  */
	for (int i = 0; i < v->count; ++i) {
		if (v->cell[i]->type == LVAL_ERR) {
			return lval_take(v, i);   // return the 1st error and free the list
		}
	}

	/*   Empty Expression   */
	if (v->count == 0)  {  return v;  }

	/*	  Single expression 	*/
	if (v->count == 1) {
		return lval_take(v, 0);
	}

	/*	  Ensure first element is Symbol   */
	lval* f = lval_pop(v, 0);
	if (f->type != LVAL_SYM) {
		lval_del(f);  lval_del(v);
		return lval_err("S-expression Doesn't start with symbol!");
	}

	/*	  Call builtin with operator   */
	lval* result = builtin_op(v, f->sym);
	lval_del(f);
	return result;
}

lval* lval_eval(lval* v) 
{
	if (v->type == LVAL_SEXPR) {
		return lval_eval_sexpr(v);
	}
	return v;
}

int main(int argc, char** argv) 
{
	mpc_parser_t* Number = mpc_new("number");
	mpc_parser_t* Symbol = mpc_new("symbol");
	mpc_parser_t* Sexpr  = mpc_new("sexpr");
	mpc_parser_t* Expr   = mpc_new("expr");
	mpc_parser_t* Lispy  = mpc_new("lispy");

	mpca_lang(MPCA_LANG_DEFAULT,
		"															\
			number   : /-?[0-9]+/;       							\
			symbol   : '+' | '-' | '*' | '/' ;						\
			sexpr    : '(' <expr>* ')';								\
			expr     : <number> | <sexpr> | <symbol>; 				\
			lispy    : /^/ <expr>* /$/ ;							\
		",
		Number, Symbol, Sexpr, Expr, Lispy);

	puts("Lispy Version : 0.0.0.5");
	puts("Use ctrl+C to quit");

	while (1) {
		char* input = readline("LIspy> ");
		add_history(input);

		mpc_result_t r;
		if (mpc_parse("<stdin>", input, Lispy, &r)) {
			lval* xx = lval_read(r.output);  // used to make sure our s-expr is working.
			lval* x = lval_eval(xx);
			lval_println(x);
			lval_del(x);
			mpc_ast_delete(r.output);
		} else {
			mpc_err_print(r.error);
			mpc_err_delete(r.error);
		}

		free(input);
	}

	mpc_cleanup(5, Number, Symbol, Sexpr, Expr, Lispy);

	return 0;
}