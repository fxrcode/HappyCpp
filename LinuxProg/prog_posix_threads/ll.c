#include <stdio.h>
#include <stdlib.h> // for malloc

typedef struct node_tag {
    struct node_tag     *link;
    int                 seconds;
    char                message[64];
} node_t;

node_t *list = NULL;

void printnode(node_t *np)
{
    printf("%d (s), mesg: %s\n", np->seconds, np->message);
}

void printll() 
{
    node_t *root;
    for (root = list; root != NULL; root = root->link)
    {
        printf("%d (s), mesg: %s\n", root->seconds, root->message);
    }
}

node_t *newNode(int seconds, char message[64])
{
    node_t *alarm, **last, *next;

    alarm  = (node_t *)malloc(sizeof(node_t));
    alarm->seconds = seconds;
    snprintf(alarm->message, sizeof(alarm->message), "%s", message);

    last = &list;
    next = *last;

    while (next != NULL) {
        if (next->seconds >= alarm->seconds) {
            alarm->link = next;
            *last = alarm;  
            break;            
        } 
        last = &next->link;
        next = next->link;
    }

    if (next == NULL) {
        *last = alarm;
        alarm->link = NULL; 
    }

    return alarm;
}

int main(int argc, char *argv[])
{
    printf("hello world~~~\n");
    node_t *tmp = newNode(50, "alarm50");    
    newNode(10, "alarm10");
    newNode(100, "alarm100");
    newNode(1, "alarm1");    
    printll();

    return 0;
}


