#include <iostream>
#include <vector>
#include <stack>
#include <limits>

using namespace std;
class Solution {
public:
    bool find132patternWRONG(vector<int>& nums) {
		int tmin = numeric_limits<int>::max();
		int tmax = numeric_limits<int>::min();
		for (int num : nums) {
			if (num < tmin) 
				tmin = num;
			if (num > tmax) 
				tmax = num;
			if (num > tmin && num < tmax) {
				cout << tmin << "->" << tmax << "=" << num << endl;
				return true;
			}
		}		        
    	return false;
	}
	void hello() {
		cout << "Hello world!" << endl;
	}
};

int main() {
	Solution sln;
	vector<int> vc = {1,0,1,-4,-3};
	cout << sln.find132pattern(vc) << endl;
	sln.hello();
	return 0;
}
