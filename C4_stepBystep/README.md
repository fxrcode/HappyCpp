## 时间
*  start: Jan 18, 2016

## 链接 
1. [三点水: "手把手教你构建 C 语言编译器"](http://lotabout.me/2015/write-a-C-interpreter-1/)
2. [C4的repo](https://github.com/rswier/c4)

## idea
* 通过最简单的C interpreter来学习一下compiler设计.
* 通过project来复习C语言.
* 提高分析代码的能力, 希望可以学完后分析出redis的架构.

## 教程1
### 使用clion. 
* 因为可以ide可以很容易的debug, 以及看structs, 调用关系, etc. 但是clion是cmake-based. 所以有些奇怪的地方.
* 编译时遇到的问题:    
    * 我在cmd里面可以直接gcc -o xc-tutor xc-tutor.c.
    * 但是在clion里面就不行, 问题在于无法调用`open()/close()/read()`. 这是为什么? 而且include的`memory.h/string.h`并没有用到. 使用clion发现需要`#include <io.h>`. 于是编译成功了. 

* 怎么debug呢?
    * 我在edit config里面设置了Program arguments为argv, 例如`./xc.exe hello.c`的话, 就定义为: `hello.c`
    * 同样是edit config里面, 设置working directory为: `D:\Coding\write-a-C-interpreter\`, 这样arguments就不用使用full path了.
    * 于是可以很好的debug了.

### xc-tutor
* 教程1的xc-tutor是三点水大大refactor c4.c, 从而一步步的理解, 设计C interpreter. 大致的步骤是:
    1. 构建VM和指令集
    2. 构建词法分析器 (what's this?)
    3. 构建语法分析器 (what's this?)

* C4就是4个methods
    1. next(): 词法分析
    2. program(): 语法分析
    3. expression(): 解析一个表达式
    4. eval(): VM用于解释目标代码

* 在第一课中, expression/eval都还是null. next只是递增指针, program只是输出每一个非空的char of code : TOKEN.

---

# How to Run the Code

```
gcc -o xc xc.c (you may need the -m32 option on 64bit machines)
./xc hello.c
./xc -s hello.c

./xc c4.c hello.c
./xc c4.c c4.c hello.c
```

# About

This project is inspired by [c4](https://github.com/rswier/c4) and is largely
based on it.

However, I rewrited them all to make it more understable and help myself to
understand it.

Despite the complexity we saw in books about compiler design, writing one is
not that hard. You don't need that much theory though they will help for
better understanding the logic behind the code.

Also I write a [series of
article](http://lotabout.github.io/2015/%E5%86%99%E4%B8%AA-C-%E7%BC%96%E8%AF%91%E5%99%A8-1/)
about how this compiler is built(in Chinese though).

# Licence

The original code is licenced with GPL2, so this code will use the same
licence.
